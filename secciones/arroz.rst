Arroz
=====

.. toctree::

   ../recetas/arroz-blanco-simple
   ../recetas/arroz-a-la-mexicana
   ../recetas/arroz-verde
   ../recetas/arroz-sushi
   ../recetas/paella-marinera
   ../recetas/jeera-rice
