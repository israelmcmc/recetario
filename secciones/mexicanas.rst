Recetas mexicanas
=================

.. toctree::
   
    ../recetas/rajas-con-queso
    ../recetas/tamales-rojos
    ../recetas/masa-maiz
    ../recetas/masa-maseca
    ../recetas/tortillas
    ../recetas/frijoles-refritos
    ../recetas/chiles-rellenos
    ../recetas/sopa-de-tortilla
    ../recetas/asado-de-boda
    ../recetas/pozole-verde
    ../recetas/tacos-de-canasta
    ../recetas/tacos-al-vapor
    ../recetas/tacos-al-pastor
    ../recetas/carnitas
    ../recetas/tortitas-espinaca-queso
    ../recetas/tortitas-de-camaron
    ../recetas/tamales-cuadrados
    ../recetas/pinole
    ../recetas/chiles-en-nogada
    ../recetas/pozole_blanco
    ../recetas/papadzules
    
