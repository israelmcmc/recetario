FROM ubuntu:xenial-20200619

RUN apt-get update -y && \
    apt-get install -y \
    python3 \
    python3-pip \
    make \
    perl \
    pandoc \
    imagemagick \
    xorg \
    libgl1-mesa-glx \
    libnss3

RUN pip3 install sphinx sphinx-rtd-theme

ADD https://download.calibre-ebook.com/linux-installer.sh ./

RUN sh linux-installer.sh

ENV PATH="/opt/calibre:${PATH}"

RUN groupadd -r user && \
    useradd -g user user

USER user

WORKDIR /home