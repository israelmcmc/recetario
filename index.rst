.. De mi Compu a Tu Cocina documentation master file, created by
   sphinx-quickstart on Thu Jun 18 10:58:43 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

De mi Compu a Tu Cocina
=======================

Download: `PDF <ebook/Recetario.pdf>`_  `EPUB <ebook/Recetario.epub>`_  `AZW3 <ebook/Recetario.azw3>`_

.. toctree::
   :maxdepth: 2

   secciones/desayunos
   secciones/arroz        
   secciones/asiaticas    
   secciones/bebidas      
   secciones/botana       
   secciones/india        
   secciones/italianas    
   secciones/mariscos     
   secciones/mexicanas
   secciones/moles       
   secciones/pan         
   secciones/pollo       
   secciones/postres     
   secciones/quesos      
   secciones/salsas      
   secciones/sopas       
   secciones/vegetarianas
   secciones/tiempos   
   secciones/ensaladas  
   
