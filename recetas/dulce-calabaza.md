Dulce de calabaza
=================


## Ingredientes

-   Calabaza

-   Cal

-   Azucar

-   Vainilla


## Instrucciones

1.  Cortar la calabaza en trozos pequeños

2.  Se deja remojar la calabaza por 24 horas en una mezcla de agua y cal
    en proporción 1 litro : 1 cucharada.

3.  Enjuagar la calabaza.

4.  Poner a cocer la calabaza a fuego lento en un mezcla de agua, azucar
    y vainilla en una proporcion de 1 litro : 1 kg : 1.5 cucharadas.

5.  Remover cuando la mayor parte del agua se haya evaporado y sólo
    quede melcocha.
