Galletas de doble chocolate
===========================


## Ingredientes

-   1 taza de harina (~145 gr)

-   1/2 cdita de polvo para hornear

-   1/2 cdita de bicarbonato de sodio

-   3/4 cdita de sal kosher (grano poco mas grueso)

-   1 1/4 barras de mantequilla (~10 cdas. o ~141
    gr)

-   3/4 taza de cacao en polvo (~75 gr)

-   1 huevo

-   2 cditas de extracto vainilla

-   1/2 taza de azúcar morena

-   1/2 taza de azúcar blanca

-   2 tazas de chispas o discos de chocolate semi-amargo (~315 gr)


## Instrucciones

1.  Acremar mantequilla con azúcar.

2.  Incorporar huevo y vainilla.

3.  Homogeneizar por separado el harina, el polvo para hornear, el
    bicarbonato, la sal y el polvo de cacao.

4.  Incorporar los ingredientes secos a la mezcla con mantequilla.

5.  Agregar las chispas/discos de chocolate y cubrir la masa con
    plástico, luego enfriar al menos 24 horas, pero no mas de 36.

6.  Hacer bolitas de 3 1/2 oz (un poco mas grandes que una
    pelota de golf) y espaciarlas bien ya que se extienden bastante al
    hornear.

7.  Hornear a 180°C (35°F) por 18 min. Enfriar en un rack.

8.  Servir tibias.
