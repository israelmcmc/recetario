Queso ricotta
=============

Basada en : [Cultures for health - Traditional whey
ricotta](https://www.culturesforhealth.com/learn/recipe/cheese-recipes/whey-traditional-ricotta-cheese/)

## Ingredientes

-   2 lt de suero de leche

-   2 cucharadas de jugo limon o vinagre

-   Sal al gusto


## Instrucciones

1.  Calentar el suero sin que llegue a hervir (~195F o 90C).

2.  Agregar el jugo de limon o vinagre y revolver.

3.  Esperar 15 min.

4.  Vaciar en un colador o estameña. Dejar escurrir for varias horas.

5.  Agregar sal al gusto.
