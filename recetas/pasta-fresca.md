Pasta fresca
============


## Ingredientes

-   180 - 200 gr de harina

-   2 huevos

-   Sal al gusto (~1/2 cucharadita)


## Instrucciones

1.  Mezclar todo y amasar hasta que este consistente (~10 min).
    Empezar con 180 gr de harina y agregar más dependiendo del tamaño
    del huvo.

2.  Envolver en plastico o toalla humeda y dejar reposar por media hora.

3.  Extender en una superficie harinada lo más que se pueda (<0.5 mm)

4.  Cortar en la forma deseada (harinar si se va a guardar).

5.  Cocer en agua hiriviendo por 3min.
