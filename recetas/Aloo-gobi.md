Aloo Gobi (curry de papas con coliflor)
=======================================


## Ingredientes

-   1 coliflor

-   3 papas medianas

-   2 cebollas moradas

-   Aceite de canola para freir

-   2 jitomates

-   Muchas especias mágicas

-   1 1/2 - 2 cucharadas de pasta de ajo y gengibre

-   1/2 - 1 cucharaditas de tumeric

-   1 - 1 1/2 cucharaditas de chile rojo en polvo

-   2 - 2 1/2 cucharaditas de comino con cilantro
    (cumin-coriander)

-   1 pizca de garam masala (< 1/2 cucharadita)

-   cilantro para decorar

- Opcional: Chiles verdes.


## Instrucciones

1.  Cortar la coliflor en floretes medianos.

2.  Freir la coliflor en aceite primero a fuego medio-alto para que se
    dore. Una vez que esté dorada reducir el fuego y tapar para que se
    suavice un poco. Tener cuidado de que no se suavice demasiado porque
    se puede cocer demaciado y desbaratarse al final.

3.  Partir las cebollas moradas en cuatro pedazos y luego cortar en
    tiras no muy delgadas (ver figura)

4.  Mientras se cocinan las cebollas partir las papas en cubos medianos.

5.  Una vez que estén cocidas las cebollas agragar las papas y cubrir.
    Dejar cocer las papas con la cebolla a fuego medio hasta que se
    suavicen, moviendo cada dos o tres minutos para que no se peguen.
    Las papas están listas cuando se pueden cortar con una espátula sin
    hacer mucha fuerza (no tienen que estar completamente cocidas ahora,
    solo un poco suaves).

6.  Ankit recomienda que al final queden cebollas a diferente nivel de
    dorado, unas cafecitas y otras no tanto, para añadirle complejidad.

7.  Cuando estén listas las papas reducir el fuego a medio bajo y
    agregar la pasta de ajo con gengibre. Hay que tener cuidado porque
    tiende a pegarse al fondo del sartén y amarga el sabor. Es
    importante revolverlo bien para que no se queme. Ankit dice que está
    listo cuando tiene un aroma dulce pero el olor a ajo ya no es tan
    intenso.

8.  Agregar las especias: primero tumeric y chile. Las papas deben verse
    de un color amarillento-naranja como en la foto.

9.  Agregar la mezcla de comino con cilantro (esta mezcla se vende en
    las tiendas indues pero se puede hacer en casa mezclando los dos
    ingredientes a partes iguales). Ankit dice que en general se le
    puede agregar mucho de este condimento a los platillos porque da
    mucho sabor pero tampoco enmascara los otros sabores.

10. Agregar el garam masala. Cuidado porque este condimento es muy
    fuerte!

11. Agregar la coliflor a las papas y revolver con cuidado para que no
    se rompan.

12. Este es el ultimo momento en el que se pueden ajustar las especias
    (excepto el chile, de ese si se puede agregar más después). Probar
    una papa y ajustar especias al gusto.

13. Cortar los jitomates en pedazos medianos-grandes. Hacer un pozo en
    medio de la cacerola y poner ahı́ los jitomates. Ankit recomienda que
    no se agreguen especias como el garam masala o el comino-cilantro
    después de agregar los jitomates a la mezcla porque tienden a soltar
    lı́quido y las especias no se 'cuecen' bien.

14. Cubrir la cacerola y dejar cocer a fuego lento. De vez en cuando se
    puede abrir para aplastar los jitomates

15. Cuando esten suaves los jitomates mezclar todo y agragar un poco de
    agua al gusto. Si se va a comer con arroz agregar más agua para que
    quede una salsa

    ![Progresión de la receta](../figures/aloo-gobi.png){#fig:aloo-gobi
    width="100%"}
