Chilate
=======

Basada en: [Cómo hacer pinole - El cuexcomate - Chilate](
http://www.cuexcomate.com/2016/03/chilate.html) y [Fernanda Berlei -
Chilate Guerrrerense](https://www.youtube.com/watch?v=xtbajMXlYno)

## Ingredientes

-   1 taza de arroz

-   180 gr de semillas cacao

-   150 gr de piloncillo

-   3 3/4 L de agua

-   1 palito de canela

-   Hielo


## Instrucciones

1.  Tostar los granos de cacao a fuego bajo por ~20 min y pelar.

2.  Poner a remojar el cacao, el arroz y la canela en agua por 1 hr.

3.  Moler lo más fino posible. Pueden ser necesarias dos pasadas.

4.  Disolver el piloncillo en agua y mezclar todo.

5.  Colar com manta de cielo

6.  Servir con hielo. Dejar caer en chorro para que form espuma.
