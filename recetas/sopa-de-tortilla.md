Sopa de tortilla {#sopa_tortilla}
================

2 porciones
20 min

## Ingredientes

-   8 tortillas rebanadas en tiras y fritas en aceite

-   1/2 cebolla

-   2 dientes ajo

-   2 tazas de consomé de pollo

-   150 gramos queso panela

-   1 aguacate

-   8 hojas epazote

-   4 tomates

-   2 chiles guajillo

-   media crema

![Media crema. Se encuentra en Target en Baltimore Ave.](../figures/media_crema.jpg)


## Instrucciones

1.  En una olla con 2 cucharadas de aceite, freír los chiles guajillo
    cortados en tiras pequeñas y reservar.

2.  Licuar los jitomates, cebolla, ajo, 8 tiras de tortilla frita y
    consomé.

3.  Agregar la salsa en la olla con el aceite caliente. Agegar las hojas
    de epazote.

4.  Agregar aproximadamente 1/2 litro de agua a la olla y hervir durante
    15 min.

5.  Checar si hace falta agua y agregar sal al gusto. La cantidad de
    agua depende de la consistencia que se quiera alcanzar.

6.  En un plato hondo colocar una porción de tortillas fritas y caldo.

7.  Decorar con aguacate, panela, crema y tiritas de guajillo.

![Producto final](../figures/sopa_tortilla.jpeg)
