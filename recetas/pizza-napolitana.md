Pizza Napolitana
================

Basada en receta de [Vito Iacopelli](https://www.youtube.com/watch?v=OjsCEJ8CWlg&ab_channel=ProHomeCooks)

## Ingredientes 

Para 2 pizzas de ~30 cm de diámetro

## Prefermento (poolish)

- 100 gr de harina
- 100 gr de agua
- 2.5 gr de levadura (~1 cucharadita)
- 2.5 gr de azúcar (~1 cucharadita)

## Masa
- Todo el prefermentp
- 250 de harina [^1]
- 150 ml de agua
- 10 gr de sal

## Pizza
- Masa
- 1 cucharada de aceite de oliva
- 2 bolas de queso mozzarella
- 1 manojo de albahaca
- ~2/3 de taza de [salsa marinara](salsa-marinara-pizza.md)

## Instrucciones

1. Prefermento
   1. Diluir la levadura y el azúcar en el agua
   1. Mezclar con la harina
   1. Dejar fermentar en el refri durante la noche
   1. Sacar a temperatura ambiente por 1hr antes de usar.
1. Mezclar todos los ingredientes de masa y amasar estirando hasta que no haya grumos. En este punto no esta lisa y se siente muy aguada.
1. Hacer una bolita, barnizar con aceite de oliva y dejar reposar por 15 min.
1. Doblar sobre si misma para hacer otra bola con la costura por debajo. En este punto ya debe de tener mejor consistencia y verse lisa. Dejar reposar 1hr. 
1. Cortarta en dos y hacer 2 bolas con la costura por debajo. Tratarla con cuidado para que no se le salga el gas y no se rompa la superficie. Dejar reposar por otra hora.
1. Poner la bola masa sobre harina por ambos lados, para que no se pegue. Cuidar de no sacar el gas.
1. Estirar y darle forma con la mano, cuidando de que el borde quede gordito y con burbujas ([así mero](https://www.youtube.com/watch?v=xzbW8CZx538)). 
1. Poner la salsa, luego la mozarella en trozos y luego hojas de albahaca [^2]. Cubrir las hojas con aceite de oliva.
1. Pasar a la pala y de ahí a la piedra en el horno (precalentada).
1. Hornear a la máxima temperatura posible hasta que este doradita. En un horno profesional a 480 C (900 F) dura 60-90 s. En un horno de casa que llega a 280 C (550 F) tarda 5-6 min.

[^1]: De preferencia harina con alto contenido de proteina (gluten). Ya sea 00, manitoba, bread flour, >12.5% proteina, W>250.
[^2]: Como los hornos de casa no llegan a altas temperaturas, una solución para que no quede aguada es cocerla un poco con sólo la salsa, y usar mozzarella que se haya dejado escurrir en trozos durante la noche.
