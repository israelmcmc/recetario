Helado de tequila
=================


## Ingredientes

-   1 taza de leche entera

-   3/4 de taza de crema para batir

-   3 yemas de huevo

-   1 1/2 caballitos de tequila (o al gusto)

-   1 pedacito de cáscara de limón

-   1/3 de taza de azúcar blanca


## Instrucciones

1.  Meter el contenedor frı́o al congelador al menos 24 horas antes de
    hacer el helado.

2.  Reservar 1/2 caballito de tequila

3.  Batir las llemas de huevo con la leche y la crema.

4.  Añadir el azúcar, la cáscara de limón y un caballito de tequila a la
    mezcla y poner a cocer a fuego lento en la estufa.

5.  Revolver la mezcla constantemente con un batidor de globo para
    evitar que se pegue al fondo o que se formen cuajos. Hay que tener
    mucho cuidado también de que no hierva la mezcla para que no se
    corte.

6.  Una vez que espese un poco (después de approx 10 min en la estufa)
    colar y vertir en un recipiente.

7.  Una vez que está lista la mezcla agregar medio caballito extra de
    tequila (o más) para obtener un sabor a tequila más intenso y
    alcohólico.

8.  Dejar reposar la mezcla al menos 3 horas para que espese un poco más
    antes de poner en la heladera.

9.  Mientras la mezcla reposa meter un recipiente al congelador donde
    pueda caber todo el helado para que cuando esté listo no se derrita
    al sacarlo de la heladera.

10. Meter la mezcla en la heladera y dejar que se bata ~20 minutos
    o hasta que esté de buena consistencia.

11. Transferir al contenedor que estaba en el congelador y dejar enfriar
    unas horas para que se termine de solidificar el helado.
