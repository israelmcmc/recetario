Pinole
======

Basada en: [Cómo hacer pinole - Cocina al Natural - Con Sonia Ortiz y
Rafael Mier](https://www.youtube.com/watch?v=ay2mmB5-18g)

## Ingredientes

-   500 gr de maiz

-   1/2 taza de azucar glass

-   3 cucharadas de canela en polvo

-   3 cucharadas de cocoa


## Instrucciones

1.  Tostar los granos de maiz a fuego medio ~10 min.

2.  Moler los granos de máiz.

3.  Revoler con los otros ingredientes y moler un segunda vez.
