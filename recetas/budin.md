# Budin

Basado en [Budín de Bolillo de Jauja](https://www.youtube.com/watch?v=H-HdgUknIAs)

## Ingredientes
- 200 de bolillo
- 1 lata leche condensada (14 oz)
- 3/4 de taza leche
- 45 gr de mantequilla
- 4 huevos 
- 1/8 de cucharadita de sal
- 1/2 de cucharadita de canela molida
- 1 cucharadita de vainilla
- 1/2 taza pasitas + 1 cucharadita de harina
- 1/2 taza de nuez picada
- 1/2 de taza de azúcar
- 1/4 de brandy, cognac o ron

## Instrucciones
1. Partir el pan en pedazitos.
1. Revolver las leches y la mantequilla. Poner a calentar, hasta que *casi* hierva
1. Agregar al pan hasta que este bien mojado. Dejar reposar hasta que se absorba bien. Probablemente no se necesite toda la leche, dependiento de que tan seco esta el pan.
1. Batir los huevos, la sal, la canela y la vainilla.
1. Espolvorear las pasitas con harina.
1. Mezclar el pan, la mezcla de huevo, las pasitas y la nuez.
1. Caramelizar el azúcar a fuego bajo y agregar al fondo del molde en donde se va a hornear.
1. Agregar la mezcla.
1. Poner en otro molde con agua caliente para hacer baño María
1. Hornear a 180 C (360 F) por 30-60 min, hasta que pase la prueba del palillo.
1. Dejar enfriar un poco y desmoldar.
