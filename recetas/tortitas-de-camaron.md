Tortitas de camarón
===================

Basada en: [Jauja - Tortitas de
camaron](https://www.youtube.com/watch?v=aD8jlQKKBWQ)

## Ingredientes

**Nopales**

-   750 gr de nopales

-   1/4 cucharadita de sal

-   1 diente de ajo

-   1/4 de cebolla

**Salsa**

-   12 chiles guajillos sin semilla

-   1 chiles ancho sin semilla

-   5 chiles puya sin semilla

-   ~10 chiles de arbol

-   3 tazas de agua

-   1/4 de cebolla

-   3 ajos

-   1/8 de cucharadita de comino

**Tortitas**

-   85 gr de camaron seco

-   3 cucharadas de pan molido

-   4 claras de huevo

-   4 yemas de huevo

-   Aceite vegetal

**Finales**

-   2 cucharadas de aceite

-   1/2 cebolla cortada en pluma

-   2 cucharadas de masa de maiz

-   1/2 taza de agua

-   1/2 manojo de cilantro


## Instrucciones

**Nopales**

1.  Cortar los nopales en tiritas

2.  Agregar la sal, la cebolla y el ajo (apachurrado).

3.  Cocer a fuego medio tapados por 15 min, moviendo un par de veces.

4.  Cocer por otros 5 - 10 min sin tapa hasta que la baba de evapore.

**Salsa**

1.  Poner a remojar los chiles en agua hirviendo por 5 min.

2.  Licuar los chiles con el agua

3.  Colar los chiles

4.  Agregar el ajo, la cebolla y el comino, y licuar bien.

**Tortitas**

1.  Quitarle los ojos al camaron

2.  Tostar ligeramente los camarones a fuego medio bajo.

3.  Licuar los camarones hasta tener un polvo fino

4.  Reservar 3 cucharadas de polvo de camarón.

5.  Agregar el pan molido al polvo de camaron restante.

6.  Batir las claras a punto de turrón

7.  Incorporar las yemas.

8.  Incorporar el camarón con el pan molido con movimiento envolventes.

9.  Calentar ~1/2 dedo de aceite a fuego medio bajo.

10. Con un cucharon poner bodoques de la mezcla a modo de tortita. Salen
    10-15 tortitas.

11. Echar aceite arriba con una cuchara y voltear cuando tengan un color
    dorado abajo.

12. Cuando tengan un color dorado en todas partes, retirar y poner sobre
    una servillata.

**Finales**

1.  Saltear la cebolla con el aceite.

2.  Agregar la salsa y el camaron molido reservado.

3.  Diluir la masa en el agua y agregar.

4.  Cocer por ~5 min, o hasta que este ligeramente espeso.

5.  Agregar las tortitas y el cilantro picado.

6.  Cocer por un par de minutos, que no se aguaden mucho las tortitas.
