Brioche
=======


## Ingredientes

-   10 gr de levadura

-   2 huevos batidos

-   1 yema (para pintar)

-   1/2 taza de ajonjoli para decorar

-   50 gr + 1 cucharadita de azucar

-   60 cc de aceite vegetal (canola)

-   1-1/2 cucharadita de sal

-   250 cc de agua tibia

-   675 gr de harina


## Instrucciones

1.  Mezclar la levadura con la cucharadita de azucar en agua tibia y
    dejar reposar ~10 min.

2.  Se mezclan el harina, la sal, el resto del azucar, el aceite y los
    huevos batidos, que al formar una mezcla homogenea, se agregan con
    el fermento de levadura.

3.  Se hace una masa suave, y en un bol engrasado, se deja reposar en un
    ambiente tibio por 2 horas.

4.  Desgasificar la masa y trabajar con harina, dividir en tres partes y
    estirar hasta formar tiras largas como de una pulgada de grosor.

5.  Hacer trenza con las tiras y dejar reposar por una hora mas.

6.  Pintar con yema y agregar ajonjoli.

7.  Hornear por ~30min a 180C (360F), hasta que dore.
