Pan de naranja
==============

Receta de Sra. Patricia Huerta, madre de la Sra. Dalia Ornelas

## Ingredientes

-   500 gr de mantequilla

-   500 gr de harina de trigo

-   8 yemas de huevo

-   8 claras de huevo

-   350 gr de azúcar

-   1 + 1/2 tazas de jugo de naranja

-   1/2 cucharadita de ralladura de naranja

-   3 cucharaditas de polvo de hornear[^1]

-   Mantequilla + harina para el molde


## Instrucciones

1.  Tritutar el azúcar en la licuadora en seco [^2]

2.  Acremar la mantequilla

3.  Agregar el azúcar y batir

4.  Agregar las yemas una a una y batir

5.  Mezclar la harina y el polvo de horner

6.  Mezclar esto con el resto, poco a poco.

7.  Batir las claras a punto de turrón

8.  Incorporar las claras a la mezcla con movimiento envolventes.

9.  Incorporar la ralladura de naranja y una taza de jugo.

10. Agregar más jugo de naranja hasta que masa se sienta fácil de batir.

11. Enharinar un molde y vaciar.

12. Hornear a 180C (360 F). hasta que pase la prueba del palillo (~
    60 min para un molde de 32 cm de diametro por 8 cm de alto, según la
    mamá de Dalia, por lo menos en Aguas).

[^1]: La receta original decía 2 cucharaditas. La mamá de Dalia cree que
    aunque funciona bien en la metropolis de Aguascalientes, para el
    nivel del mar es mejor usar 1 cucharadita más. En alguno sitios
    recomiendan usar el doble para esas alturas.

[^2]: Dalia creo que sólo es para hacer más fácil si mezcla a mano.
    Quizá se pueda usar azúcar glass directamente?
