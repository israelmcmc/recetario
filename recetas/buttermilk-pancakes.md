Buttermilk pancakes
===================


## Ingredientes

-   1 1/4 tazas de harina

-   2 claras de huevo

-   2 yemas de huevo [^1]

-   1 1/4 tazas de buttermilk (como substituto de
    3/4 a 1 tazas de leche[^2] con 2 cucharadas de vinagre y
    dejar por 5 min)

-   1/4 de taza de azucar

-   1/4 de taza de aceite

-   1 cucharadita de polvo para hornear

-   1 cucharadita de bicarbonato

-   1 cucharada de vainilla

-   Mantequilla para engrasar


## Instrucciones

1.  Batir las claras a punto de turrón

2.  Mezclar el resto de los ingredientes

3.  Incorporar las claras batidas en forma envolvente.

4.  Hacer los hotcakes a fuego medio en un sarten con mantequilla.
    Voltear cuando este burbujeando y las orillas esten cocidas. Cuando
    se volteen deben de tener un color dorado claro, ajustar la llama
    para que esto pase.

[^1]: Dejar sólo una si se quiere menor sabor a huevo

[^2]: Dependiendo de que tan gorditos los quieres
