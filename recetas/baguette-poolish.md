Baguettes con poolish
=====================

Fuente: How to make bread por Emmanuel Hadjiandreou


## Ingredientes

**Ingredientes para el prefermento**

-   1/4 de cucharadita de levadura (2g)

-   125 ml de agua tibia

-   125 gr de harina

**Ingredientes para el resto**

-   1/4 de cucharadita de levadura (2g)

-   300 gr de harina

-   5 gr de sal

-   140 ml de agua tibia


## Instrucciones

1.  Preparar el pre-fermento

    -   Diluir la levadura en el agua tibia

    -   Mezclar la harina

    -   Dejar reposar cubierta en plástico durante la noche (o 4 - 8
        hrs).

2.  Mezclar la sal con la harina

3.  Diluir la levadura en el agua tibia.

4.  Mezclar el agua con la harina y amasar hasta que esta lisa. Puedes
    trabajar en una superficie enharinada, pero cuidando de no usar
    mucha.

5.  Dejar reposar en un molde cubierto con plástico por ~1hr
    (hasta que doble su tamaño).

6.  Dividir en 4 pedazos de 170 gr. Cada pedazo se alarga en una
    superficie con harina, doblandose sobre si mismo, hasta que tenga
    ~20 cm de largo.

7.  Dejar reposar en dobleces en una manta para pan (couche / proofing
    linen), con el dobles hacia abajo, por ~1hr (hasta que doble
    su tamaño.)

8.  Pasar a una pala con el dobles hacia abajo.

9.  Hace cortes con una navaja de afeitar, de manera oblicua.

10. Pasar a la piedra en el horno, precalentado a 240 C (475 F), y
    hornear por 15 min. Rociar agua a las parades del horno durante los
    primeros 6 min, cada 1-2 min[^1].

[^1]: De forma alternativa puedes calentar un sarten de hierro fundido,
    ponerlo en el fondo y vaciar agua.
