Caldo tlalpeño
==============


## Ingredientes

-   6 tazas de agua

-   1 chile chipotle (o al gusto)

-   1 cucharadita de sal

-   de cucharadita de pimienta

-   120 gr de pechuga de pollo

-   2 zanahorias cocidas

-   2 aguacats

-   1 limón (o al gusto)

-   160 gr de garbanzo cocido

-   160 gr de arroz blanco cocido

-   250 gr de queso panela


## Instrucciones

1.  Se coce la pechuga. Se cuela el agua pero no se tira.

2.  Licua el agua, el chipotle, la sal y la pimienta.

3.  Se desmenuza la pechuga.

4.  Se agrega el resto y se calienta.
