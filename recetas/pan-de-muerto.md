Pan de muerto
=============

Basada en : [Yuri de Gortari - Pan de
Muerto](https://www.youtube.com/watch?v=PnYVBsY-xAk)

## Ingredientes

**Masa**

-   90 gr de mantequilla +  barniz

-   2 huevos

-   200 ml de leche tibia

-   150 gr de azucar + espolvoreado

-   La ralladura de una naranja

-   15 gr de levadura

-   1 cucharaditas de vainilla

-   500 gr de harina de trigo

- 1 pizca de sal


## Instrucciones

1.  Poner la levadura en leche tibia, con una cucharada de azucar y una
    de harina, haciendo una especie de atolito. Dejar fermentar en un
    ambiente caliente, (e.g. sobre el horno), hasta que duplique su
    tamaño.

1. Acremar la mantequilla.

1. Mezclar la sal, harina, azúcar y ralladura.

1. Batir la leche con levadura, los huevos y vainilla.

1. Mezclar todo y amasar hasta que despeque.

1. Dejar reposar masa en un bowl ligeramende engrasado hasta que duplique su tamaño (~1 hr).

1. Formar 15 bolas de 50 gr.

1. Armar los adornos. 20 gr por pan. 2x(o-o-o-o) para los cruz + o para el botón.

1. Dejar reposar por ~1hr hasta que doble su tamaño.

1. Hornear a 180C (350F) por 12 min (hasta que se empiece a
    dorar la superficie.)

1.  Barnizar con una capa delgada de mantequilla derretida.

1.  Espolvorear azucar.


