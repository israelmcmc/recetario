Ensalada caballa y betabel 
==============

Basada en: comida dominguera improvisada

## Ingredientes

-   5 betabeles medianos

-   1 lata de caballa en conserva, preferentemente en aceite de oliva

-   1/4 de taza de nuez entera

-   70g de queso de cabra

-   1/4 cdita de genjibre recien rallado

-   1 cdita de aminos de coco (o salsa de soya)

-   2 cdas de vinagre balsamico

-   2 cdas de aceite de oliva extra virgen

-   1/4 cdita de mostaza amarilla

-   Sal al gusto


## Instrucciones

1.  Se prepara un aderezo mezclando el genjibre, los aminos, el aceite de oliva, la sal, la mostaza, y el vinagre balsamico mezclando todo hasta emulsificar.

2.  Los betabeles se cuecen al vapor hasta que esten suaves, luego se pelan y se rebanan en rodajas delgadas (1/2 cm aproximadamente).

3.  Se incorpora el queso de cabra, la nuez en trozos de medio centimetro mas o menos, y el caballa en conserva con su aceite hasta lograr un tipo de pate.

4.  Sobre un plato, se acomodan las rodajas de betabel, encima se agrega la mezcla del queso, nuez y caballa.

5.  Finalmente, se adereza con la vinagreta al gusto. Se recomienda comer con un buen pan tostado.