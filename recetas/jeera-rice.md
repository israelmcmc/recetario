# Jeera rice

Basado en [Richa's Perfect Jeera Rice (Indian Cumin Rice)](https://myfoodstory.com/perfect-jeera-rice-recipe/)

## Ingredientes
- 1 taza de arroz (de preferencia basmati)
- 2 tazas de agua caliente
- 1/2 - 1 cucharada de ghee (o mantequilla)
- 1 hoja de laurel
- 1 pedacito de canela
- 1/4 - 1/2 cucharadita de semillas de comino
- 1 chile verde (delgado, como chile de árbol verde)
- 3/4 de cucharadita de sal

## Instrucciones
1. Guisar el chile y las especias en el ghee por ~1 min. 
1. Guisar el arroz por ~2 min.
1. Agregar el agua y la sal. Cocer tapado por ~20 min hasta que el agua se evapore.
1. Dejar reposar por unos minutos antes de servir.
