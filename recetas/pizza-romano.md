Pizza Romano
============

**Ingredientes (3 pizzas de 33 cm)**

-   3gr de levadura seca activa

-   75gr de agua tibia

-   225gr de harina de trigo

-   225gr de harina de trigo entero

-   225gr de agua fría

-   9gr de sal

-   200 gr de queso provolone dulce

**Instrucciones para masa**

1.  Revolver la levadura en el agua caliente y dejarla por 10min

2.  Revolver ésta última con la harina

3.  Agregar el agua fría y revolver

4.  Agregar la sal y amasar.

5.  Poner en molde, con un fina capa de agua y cubrir. Dejar reposar por
    un día.
