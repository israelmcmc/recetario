Chilaquiles verdes
==================

Basada en: [CHILAQUILES VERDES - Vicky Receta
Facil](https://www.youtube.com/watch?v=CcuHrqMZOFU)
Igual que [chilaquiles rojos](chilaquiles-rojos.md) excepto la salsa.


## Ingredientes

**Salsa**

-   300 gr de tomate verde

-   ~6 ramitas de cilanto

-   4 chiles verde (o al gusto)

-   1 rebanada de cebolla

-   1 rama de epazote fresco

-   1 diente de ajo

-   1 cubo de caldo de pollo

-   Sal al gusto


## Instrucciones

**Salsa**

1.  Cocer los tomates y el chile hasta que cambiend de color

2.  Licuar todo

3.  Cocer por ~10min. Agregar agua si es necesario
