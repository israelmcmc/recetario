# Chiles en nogada

Basada en [Tu Cocina (Yuri de Gortari) - Chiles en nogada](https://www.youtube.com/watch?v=6TFSKA5YjIk)

## Ingredientes

### Chiles, capeado y adorno

- ~12 chiles poblanos
- ~7 huevos
- Harina
- Aceite
- 2 granadas
- Perejil picado


### Relleno

- 3/4 de taza de cebolla picada
- 3 dientes de ajo
- 350 gr de cerdo
- 350 gr de res
- 4 jitomates
- ~1/2 de cucharadita de orégano
- ~1/2 cucharadita de tomillo
- ~1/2 cucharadita de clavo molido
- ~1/2 de cucharadita de canela
- ~1/2 de cucharadita de pimienta
- ~3/4 de cucharadita de sal
- 3/4 de taza de pasitas
- 3/4 de taza de acitrón (o biznaga cristalizada)
- 1/2 de taza de almendra pelada
- 3/4 de taza de piñon
- 4 duraznos
- 2 peras
- Aceite o manteca

### Crema

- 1 kg de nuez de castilla
- Leche
- 500 gr de almendra pelada
- 200 gr de queso de cabra
- 3/4 de taza de jerez
- 1 cucharada de azúcar
- 1 pizca de sal

## Instrucciones

### Relleno
1. Acitronar la cebolla
1. Agregar y el ajo y freir por 1min.
1. Agregar la carne y guisar
1. Agregar la jitomates y guisar hasta que se desbaraten.
1. Agregar la sal y especias. Cocer por 1 min.
1. Agregar la almendra pelada y picada.
1. Agregar toda la fruta y cocer hasta que este suave.

### Crema
1. Licuar las nueces y almendras con un poco de leche.
1. Licuar con el resto de los ingredientes. Usar la cantidad de leche adecuada para que quede como salsa, pero no muy aguada.

### Chiles, capeado y adorno
1. Seguir los mismos pasos que para [los chiles rellanos](chiles-rellenos.md)
1. Adornar con granada y perejil.


