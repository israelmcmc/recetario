# Pastel "A Kris le gusta la Nutella"

Basado en [Nutella fudge - Pies and tacos](https://www.piesandtacos.com/nutella-fudge/), [Nutella frosting - Sally's baking addiction](https://sallysbakingaddiction.com/nutella-frosting/) y [Vanilla Cake - Sally's](https://sallysbakingaddiction.com/vanilla-cake/)

## Ingredientes

### Relleno
- 2 latas de leche condensada (latas de 14 oz)
- 120 gr de Nutella
- 28 gr de mantequilla sin sal (2 cucharadas)

### Betun
- 225 gr de mantequilla
- 4 tazas (460g) de azúcar glass
- 3/4 de taza (225g) de Nutella
- 1/3 de taza (80ml) de crema para batir (heavy cream)
- 1 cucharadita de vainilla
- 1 pizca de sal

### Pan
- 420 gr (3 2/3 de taza) de harina
- 1/4 de cucharadita de sal
- 1 cucharadia de polvo par hornear
- 3/4 de cucharadita de bicarbonato
- 345 gr (1 1/2 taza) de mantequilla (+ para encerar)
- 400 gr (1 tazas) de azúcar
- 3 huevos enteros
- 2 claras de huevo
- 2 cucharaditas de vainilla
- 1/2 cucharadita de extracto de almendra
- 1 1/2 tazas (360 ml) de buttermilk (suero de mantequilla o mazada)

### Otros
- Ojuelas de almendra.
- Ferreros

## Instrucciones

### Relleno

1. Calentar a fuego medio-bajo hasta que hierva
2. Sin dejar de menear y raspando el fondo, hervir hasta que este espeso como un cajeta (~10 min). Nota: Al enfriarse se va a espesar considerablemente más, y si te pasas queda duro como un paleta.

### Betun
1. Acremar la mantequilla
2. Mezclar con la Nutella
3. Agregar la vainilla, crema y sal.
4. Incorporar y batir el azúcar de a poco a la vez. 

### Pan
1. Acremar la mantequilla
2. Mezclar con el azucar hasta que este homogéneo
3. Agregar loa huevos y las claras hasta que esté homogéneo
4. Mezclar los ingredientes secos
5. Agregar y batir la leche
6. Dividir en tres moldes redondos. Estos deben de tener matenquilla en el fondo y una capa de papel encerado.
7. Hornear a 350 F (180 C) por 20-25min, hasta que pase la prueba del palillo.

### Armado
1. Apilar pan y relleno con un cinturon de plastico.
2. Meter al refri un rato
3. Forrar con betun
4. Empanizar con la almendra.
5. Poner unos Ferreros arriba.
