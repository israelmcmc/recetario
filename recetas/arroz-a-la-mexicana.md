Arroz a la mexicana
===================


## Ingredientes

-   2 tomates medianos (~400gr)

-   2 cucharaditas de aceite

-   1 taza de arroz

-   2 tazas de agua

-   Cubo de caldo de pollo (11gr)

-   Cuarto de cebolla (~70gr)

-   de cucharadita de sal (o al gusto)

-   de cucharadita de ajo en polvo (o al gusto)

-   Verdura cocida picada al gusto (zanahoria, elote o chicharo)


## Instrucciones

1.  Se hierven los tomates hasta que la cáscara se abra (~10min),
    se pelan y muelen.

2.  *Opcional*: Se lava el arroz hasta que el agua salga relativamente
    clara (~3 veces).

3.  Se pone a hervir las dos tazas de agua y se disuelve el cubo de
    pollo.

4.  Se precaliente un sartén con el aceite y se añade el arroz. Se
    mantiene en movimiento hasta que el arroz este amarillo. Se agrega
    la cebolla picada y se continua moviendo hasta que tenda un color
    dorado sin quemarse.

5.  Se agrega y revuelve en el sartén el tomate, la sal, el ajo y el
    agua con el cubo de pollo.

6.  Se deja a fuego lento, hasta que toda el agua se haya evaporado pero
    antes de que se pegue el arroz. Agregar las verduras un poco antes
    de esto si se desea.
