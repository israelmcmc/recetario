Tzatziki
========


## Ingredientes

-   250gr de yogurt griego (e.g. Fage)

-   1/2 pepino grande

-   2 dientes de ajo finamente picados

-   1/2 cucharada de eneldo (*dill*) finamente picado.

-   1 cucharada de vinagre rojo/tinto

-   2 cucharadas de aceite de oliva

-   1/4 de cucharadita de sal


## Instrucciones

1.  Pelar el pepino y partirlo finamente. Si se tiene tiempo dejar secar
    para mejorar consistencia.

2.  Revolcer el resto. Si se tiene tiempo dejar reposas en el
    refrigerador por unas horas.
