Tortillas
=========


## Ingredientes
Masa (de [maseca](masa-maseca.md) o [nixtamal](masa-maiz.md))

## Instrucciones

1.  Precalentar comal a fuego alto. De preferencia uno grueso para que
    el tenga una temperatura uniforme.

2.  Aplanar con tortillera y bolsa de plastico delgado (de la sección de
    frutas y verduras).

3.  Dejar en el comal por 15 - 30 sec, hasta que cambie ligeramente de
    color, y voltear.

4.  Dejar en el comal por 90 - 120 sec, hasta que se vea "seca" (con
    "bombitas") y voltear.

5.  Esperar a que se infle y dejar por 10 - 15 s más. Si no se infla
    sóla, una presión suave con la mano ayuda.

6.  Sacar del comal.
