Conchas
=======


## Ingredientes
**Masa**

-   250 gr de harina

-   5 gr de sal (~1 cucharadita)

-   7 gr de levadura (~2 1/2 cucharaditas)

-   2 huevos + barniz

-   50 gr de mantequilla sin sal (a temperatura ambiente)

-   50 ml de agua tibia

-   50 ml de agua fria

-   40 gr de azucar

**Tapa blanca**

-   100 gr de manteca vegetal

-   100 gr de azucar glass

-   100 gr de harina

**Tapa de chocolate**

-   100 gr de manteca vegetal

-   100 gr de azucar glass

-   16 gr de cocoa

-   84 gr de harina

## Preparación

1.  Mezclar la levadura con el agua tibia para activarla

2.  Amasar todo hasta que despegue, estirando y con ayuda de una
    espátula (a mano toma ~1 hora, en KitchenAid ~10 min)

3.  Dejar fermentar hasta que doble el tamaño

4.  Hacer bolas de 50gr.

5.  Untar con un poco de manteca vegetal.

6.  Hacer la mezcla de las tapas

7.  Hacer las tapas (25gr) con ayuda de harina para que no se pegue.
    Consejo: Hacer bolitas y usar la prensa de tortillas para
    aplastarlas es más fácil que usar rodillo y cortador.

8.  Acomodar tapa y marcar (enharinar el marcador).

9.  Hornear a 200C (400F) por 12min.
