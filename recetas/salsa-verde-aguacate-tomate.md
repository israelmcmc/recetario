Salsa verde aguacate y tomate
=============================


## Ingredientes

-   300 gr de tomatillos (5-10 tomates, dependiendo del tamaño)

-   1 aguacate

-   1 rebanada de cebolla (~50gr)

-   2 dientes de ajo

-   Chile verde o jalapeño al gusto (~5 chiles verdes)

-   1 cucharadita de sal

-   Un manojo de cilantro ( 10 gr ya sin el tallo)

-   1 limón


## Instrucciones

1.  Cocer en agua los tomates, chile, ajo y cebolla por ~10 min.

2.  Licuar todo.
