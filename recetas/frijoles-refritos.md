Frijoles refritos
=================


## Ingredientes

-   450 gr de frijoles

-   2 cucharaditas de sal

-   1-1 taza de aceite

-   3 chiles verdes

-   1 rodaja de cebolla

-   3 dientes de ajo aplastados

-   1 tortilla


## Instrucciones

1.  Cocer los frijoles

    -   Olla a presión: se ponen a cocer con dos litros de agua (marca
        del medio en una olla de 4 cuartos). Se deja por 20 min una vez
        que empieza a bambolear.

    -   Olla normal: se dejan remojando los frijoles la noche anterior y
        se ponen a cocer a fuego medio con 3 litros de agua hasta que
        tengan la consistencia deseada.

2.  Poner a calentar el aceite a fuego medio y agregar el ajo, los
    chiles, la cebolla y la tortilla.

3.  Cocer hasta que la tortilla se haya tostado, apagar y remover todo.

4.  Agregar los frijoles sin el agua y machacar.

5.  Agregar agua donde se cocieron los frijoles hasta que tenga la
    consistencia deseada.

6.  Agregar la sal.

7.  Prender y menear hasta que de un hervor.
