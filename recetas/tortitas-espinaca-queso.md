Tortitas de espinaca con queso
==============================

Basada en: [Jauja - Tortitas de Espinaca con Queso sin
Capear](https://www.youtube.com/watch?v=r7K9CjKKr6k)

## Ingredientes

**Caldo**

-   300 gr de tomate (2 o 3)

-   3 chiles serranos

-   2 rebanadas de cebolla en plumas (~80 gr)

-   1 diente de ajo picado

-   1 pizca mejorana

-   1 pizca tomillo

-   1 hoja laurel

-   caldo de pollo ( ~1/2 cubito)

-   1 pizca de sal

-   ~1 cucharada de aceite

**Tortitas**

-   300 gr de espinaca

-   100 gr de queso rallado (e.g. manchego, chihuahua)

-   1 pizca de sal

-   1 pizca de pimienta

-   1 huevo

-   2 rebanadas de cebolla picada (~80 gr)

-   1 cucharada de harina de trigo

-   1 cucharadita de crema

-   ~1 cucharada de aceite


## Instrucciones

**Caldo**

1.  Hervir los tomates y el chile hasta que hasta que revienten los
    jitomates

2.  Licuar y colar.

3.  Sofreir la cebolla.

4.  Agregar lo licuado y el resto de los ingredientes.

5.  Agregar un poco de agua en donde se cocieron los tomaes y hervir por
    unos minutos.

**Tortitas**

1.  Cocer la espinaca por ~5 min.

2.  Exprimir la espinaca

3.  Sofreir la cebolla.

4.  Mezclar todo.

5.  Hacer 4 tortitas y dorarlas con poco aceite a fuego medio alto.

6.  Servir con el caldo.
