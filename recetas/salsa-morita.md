# Salsa de chile morita

Basada [receta de salsa morita tipo taquera de RecetasGratis.com](https://www.recetasgratis.net/receta-de-salsa-de-chile-morita-tipo-taquera-25839.html)

## Ingredientes

- 1/2 kg de tomate verde
- 30 gr de chile morita
- 15 gr de chile de árbol
- 6 dientes de ajo
- 1 cuchadita de sal
- 1/4 taza de vinagre de manzana

## Instrucciones

1. Cocer los chiles, el tomate y el ajo en agua hirviendo hasta que los tomates cambien de color[^1].
1. Agregar la sal, el viangre y licuar.

[^1]: Cocer mucho los tomates los vuelve todavía más ácidos de lo que ya son.