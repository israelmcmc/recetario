# Sopa de poro y papa

## Ingredientes
- 2 rebanadas de cebolla
- 2 dientes de ajo
- 1 jitomate
- ~5 cucharadas de aceite
- 1 cucharadita de sal
- 2 papas (400-500 gr)
- 2-3 poros (600-800 gr)
- 1.5 L de caldo de pollo

- 1 cucharadita de orégano

## Instrucciones
1. Azitronar la cebolla y el ajo finamente picados. A fuego bajo y tapado. Agregar la sal cuando se empiecen a suavizar.
1. Rallar el jitomate y reducir, a fuego bajo.
1. Agregar la papa picada y el orégado. Revolver.
1. Agregar el caldo y dejar hervir hasta que la papa se suavice (~5 min).
