Conos de hojaldre
=================

También llamados caracoles, cuernitos, canutillos o tornillos, rellenos
de crema pastelera.
Receta para 12 conos de 5 pulgadas.

## Ingredientes

-   450 gr de pasta de hojaldre

-   ~1 1/2 tazas de [crema pastelera](crema-pastelera.md)

-   1 huevo batido

-   Azúcar para espolvorear


## Instrucciones

1.  Con un rodillo extender la masa hasta formar un cuadrado de
    aproximadamente 50 cm por lado [^1]. Cortar orillas.

2.  Dividir en 12 tiras.

3.  Con una brocha poner una capa fina de huevo en una orilla, que cubra
    la mitad de la tira a lo largo. Esto es para que se pegue bien la
    masa.

4.  Enroscar en el cono de metal, cuidando de que la parte con huevo
    quede sobre la masa y no sobre el cono de metal. Apretar al inicio y
    al final para sellar.

5.  Pintar con huevo y empanizar con azúcar (poniendo en azúcar en un tazón, como churros).

6.  Hornear a 200 C ( 400 F) por 15-20 min (hasta que todos los conos
    esten bien doraditos).

7.  Remover con un giro el cono de metal cuando estén tibios.

8.  Rellenar con la crema pastelera.

[^1]: Si se van a hacer por partes, mantén en el refrigerador la porción
    de masa que no se va a usar al momento. Si no se derrite la
    mantequilla y no se alza.
