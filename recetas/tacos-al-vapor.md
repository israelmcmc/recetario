Tacos al vapor
==============

Basada en: [como hacer TACOS DE CANASTA, la receta secreta de los
taqueros, \# 456 \| Chef
Roger](https://www.youtube.com/watch?v=kH0nLhOtY2s) y [TACOS AL VAPOR
ESTILO JALISCO - Alejandra de
Nava](https://www.youtube.com/watch?v=PoDRiJ3zVmc)
La preparación de los guisos y el aceite es igual que los [tacos de canasta](tacos-de-canasta.md), sin embargo
el procedimiento para armar los tacos es diferente. La principal
diferencia es que en los tacos de canasta todo tiene que estar caliente
al poner en la canasta ya que generan su propio vapor. En los tacos al
vapor no es indispensable que los tacos estén calientes ya que se usa
una vaporera.

## Ingredientes
Ver receta de

## Instrucciones

1.  Pasar la tortilla por aceite por ambos lados[^1].

2.  Poner guiso y cerrar.

3.  Acomodar en la vaporera capa por capa, tratando de dejar un hoyo en
    el centro por donde pueda circular el vapor. La primer y última capa
    són sólo de tortillas entendidas, también pasadas por aceite, para
    proteger los tacos.

4.  Tapas con una toalla y luego con la tapadera.

5.  Prender la vaporera y apagar 15 min después de que empezó a salir el
    vapor.

6.  Dejar reposar por 15-30 min.

[^1]: Es indispensable que estén completamente pasadas por aceite para
    que no se rompan, no trates de hacer light. De preferencia usar
    tortillas recién hechas para que no se rompan.
