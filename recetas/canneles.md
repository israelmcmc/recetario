Cannelés (receta de Thomas Bouillier)
=====================================


## Ingredientes

-   litro de leche

-   1 cucharada de mantequilla

-   3 cucharadas de vainilla

-   250 gr de harina

-   500 gr de azucar morena

-   4 huevos

-   2 yemas de huevo

-   4 tapitas de ron


## Instrucciones

1.  Precalentar el horno a 420 F.

2.  Derretir la mantequilla en la leche con vainilla.

3.  Mezclar la harina y el azucar.

4.  Agregar los huevos a los ingredientes secos y batir hasta que esté
    uniforme.

5.  Agregar el ron a la mezcla de harina con huevo.

6.  Mezclar la harina con la manteca vegetal hasta que esté bien
    integrada.

7.  Incorporar la leche con la masa. Primero se pone tantita leche
    (~1/4)y se mezcla con la masa y luego se agrega el resto de la
    leche.

8.  Thomas dice que salen mas sabrosos si se deja reposar la masa en el
    refri aunque el nunca lo hace.

9.  Engrasar moldes de cannelets con mantequilla.

10. Vertir la masa en los moldes, dejando ~0.5-1 cm. de espacio
    para que crezcan.

11. Hornear 40 minutos o hasta que estén dorados.
