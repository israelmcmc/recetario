Chilaquiles rojos
=================

Basada en: [CHILAQUILES ROJOS - Vicky Receta
Facil](https://www.youtube.com/watch?v=I8xJOKFjDF4)

## Ingredientes

**Salsa**

-   4 jitomates

-   3 chiles huajillo

-   Chile de arbol al gusto ( ~1/2 puño)

-   Sal al gusto ( ~1/2 cucharadita)

-   1 diente de ajo

-   1 rebanada de cebolla

-   1 cubo de consome de pollo

**Totopos**

-   1/2 kg de tortilla

-   Aceite vegetal

**Para acompañar**

-   Crema

-   Queso seco

-   Cebolla con limon


## Instrucciones

**Salsa**

1.  Cocer los tomates hasta que reviente

2.  Remover las semillas del chile huajillo.

3.  Cocer todos los chiles hasta que se aguaden.

4.  Licuar todo

5.  Hervir por ~10-15min. Agregar agua si es necesario.

**Totopos**

-   Dejar las tortillas afuera por ~1dia para que se sequen

-   Cortar en 6

-   Poner en aceite caliente hasta que se doren.

-   Dejar escurrir.

**Chilaquiles**

-   Hacer la salsa.

-   Hacer los totopos. De preferencia hacerlos hasta que ya este lista
    la salsa.

-   Bañar los totopos con las salsa y revolver bien en un sartén.

-   Cubrir con crema, queso y cebolla desflemada.
