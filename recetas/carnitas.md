Carnitas
========

Basada en: [La ruta del sabor - Carnitas estilo
Michoacan](https://www.youtube.com/watch?v=RHBXS7Oo1VM) y [Carnitas
estilo michoacan - Chef-roger
style](https://www.youtube.com/watch?v=3Yz4mSFP9G8).

## Ingredientes

-   1 kg de maciza (en pedazos grandes)

-   1 kg de buche

-   1/2 kg de cuerito

-   2 1/2 kg de manteca

-   1/2 litro de agua

-   1/2 litro de agua con una cucharada de sal (salmuera)


## Instrucciones

1.  Calentar la manteca a fuego alto hasta que este bien caliente.
    Prueba con un pedazo de cuero primero, debe de burbujear fuertemente
    al meterlo si esta suficientemente caliente.

2.  Agregar la maciza y deja hasta que este dorada por fuera (~10
    min). Mover ligeramente para que se doren parejo y no se quemen.

3.  Agregar el buche y dejar dorar ~1 min.

4.  Agregar el agua. En este punto la manteca ya no debe de estan can
    caliente.

5.  Dejar a fuego medio por 1 hr, moviendo ocasionalmente.

6.  Poner los cueritos en la superficie, tapando la carne.

7.  Agregar la salmuera.

8.  Dejar a fuego medio por ~1 - 1.5 hr, moviendo ocasionalmente.
    Comenzar a checar el cuerito a la hora y apagar cuando este suave.
