Muffins de banana
=================


## Ingredientes

-   de taza de leche

-   1 cucharada vinagre

-   3 plátanos

-   1 taza de azucar

-   2 huevos

-   de taza de aceite vegetal

-   2 tazas de harina

-   2 cucharaditas de bicarbonato de sodio


## Instrucciones

1.  Mezclar la leche con el vinagre y deja reposar por 10 min.

2.  Mezclar el resto de los ingredientes en el orden en el que aparecen

3.  Hechar tres cucharadas de la mezcla de leche y vinagre en la mezcla
    principal, y mezclar.

4.  Hornear a 180°C (350°F) hasta que este firme
    (~20-30min).
