Masa de maíz nixtamalizado
==========================

Basada en [receta para masa de nixtamal de México Tierra
Viva](https://mexicotierraviva.org/con-maiz-masa-de-nixtamal/)

## Ingredientes (~1 kg de masa, o ~20 tortillas)

-   1 kg de granos de maiz seco

-   2 1/2 litro de agua

-   2 1/2 cucharada de cal

-   ~300 ml de agua extra


## Instrucciones

1.  Diluir la cal en el agua y gregar el maiz

2.  Calentar a fuego alto hasta que de un hervor[^1].

3.  Dejar reposar por ~8 hr.

4.  Lavar varias veces, frotando los granos unos con otros, hasta que
    salga clara el agua ( ~4 cambios de agua)

5.  Moler lo más fino posible

6.  Agregar el agua y amasar. Eentre menos seca mejor pero sin que se
    pegue a las manos torteando.

[^1]: Hervirlo por unos cuantos minutos más esta bien, aunque no es
    necesario. Si se coce por mucho que "apozola" y ya no jala bien.
