Pastel de zanahoria de la mamá de Paco
======================================


## Ingredientes (Doble porcion)

-   4 1/2 tazas de harina

-   3 tazas de azucar

-   2 cucharaditas de bicarbonato

-   2 cucharaditas de sal

-   2 1/2 tazas de aceite vegetal

-   8 huevos

-   1 cucharadita de nuez moscada

-   6 tazas de zanahoria rallada (~1 kg)

-   2 tazas de nuez molida


## Instrucciones

1.  Se baten por separado huevos, aceite y azucar, hasta incorporar en
    una mezcla cremosa.

2.  Se mezclan la zanahoria y la nuez por separado hasta incorporar
    todo.

3.  Se cierne harina, sal, bicarbonato, y nuez moscada en una mezcla
    homogenea seca.

4.  Se incorpora la mezcla seca a la humeda poco a poco y finalmente se
    agrega la zanahoria con la nuez, hasta lograr una consistencia poco
    fluida.

5.  Se engrasa un molde grande y se hornea a 180° (350°F)
    por ~45-50 min, o hasta que salga un palillo limpio.
