Zarandeado
==========

Basada en : [Pescado zarandeado estilo Sinaloa - APRENDE A COCINAR Con
el Chef
Vizcaino](https://aprendeacocinarfacil.wordpress.com/2012/03/13/pescado-sarandeado-estilo-sinaloa-aya-pinchi/)

## Ingredientes

-   Un pescado de 2-3 kg _con_ escamas [^1]

-   1/2 taza de mayonesa

-   2 cucharadas mostaza

-   1/2 cucharada de apio en polvo

-   1/2 cucharada de ajo en polvo

-   1/2 cucharada de cebola en polvo

-   [Salsa para zarandeado](salsa-zarandeado.md)

-   Sal


## Instrucciones

1.  Abrir el pescado, dejando todo (incluyendo escamas) excepto las
    vísceras, como se muestra en la Fig.
    [1](#fig:corte-zarandeado){reference-type="ref"
    reference="fig:corte-zarandeado"}

    1.  Hacer un corte largo desde la aleta dorsal (Fig.
        [1](#fig:corte-zarandeado){reference-type="ref"
        reference="fig:corte-zarandeado"}a). El cuchillo pasa por un
        lado de las espinas traseras y corta las costillas.

    2.  Con un cuchillo grande y un mazo partir el craneo en dos, a lo
        largo (Fig. [1](#fig:corte-zarandeado){reference-type="ref"
        reference="fig:corte-zarandeado"}b-c).

    3.  Remover las vísceras con las manos, incluyendo agallas[^2] (Fig.
        [1](#fig:corte-zarandeado){reference-type="ref"
        reference="fig:corte-zarandeado"}d).

    4.  Con el cuchillo y el mazo romper cortar la columna vertebral
        después de la cabeza antes de la cola (Fig.
        [1](#fig:corte-zarandeado){reference-type="ref"
        reference="fig:corte-zarandeado"}e-f).

    5.  Hacer el mismo corte largo por el otro lado del pescado, también
        pasando por un lado de las espinas traseras y cortando las
        costillas (Fig. [1](#fig:corte-zarandeado){reference-type="ref"
        reference="fig:corte-zarandeado"}g).

    6.  Opcional: quitar las espinas de las costillas con unas pinzas.

    7.  Lavar. El pescado debe de quedar en forma de mariposa, con la
        vertebra y las espinas traseras por un lado (Fig.
        [1](#fig:corte-zarandeado){reference-type="ref"
        reference="fig:corte-zarandeado"}h).

2.  Hacer cortes transversales a la lonja del pescado para que entre el
    adobo.

3.  Salar el pescado al gusto

4.  Mezclar la mayonesa, la mostaza, y los polvos. Adobar el pescado.

5.  Asar en la zaranda con fuego alto[^3] por el lado de la carne por
    15 - 20 min, hasta que este bien dorado.

6.  Abrir la zaranda y cubrir con la salsa. Cuidar que la zaranda no se
    traiga pedazos de pescado, ir despegando poco a poco con una cuchara
    de ser necesario.

7.  Poner otra vez al fuego por el otro lado por ~20 min más. Las
    escamas deben de quedar completamente quemadas.

![Corte de pescado para zarandear. [Video de Robles Ponce](https://www.youtube.com/watch?v=_OmdFQWyWRc)](../figures/corte-zarandeado.png){#fig:corte-zarandeado width="\\textwidth"}

[^1]: Pargo o Huachinango, Mero o Cabrilla, Lobina, Curvina, Pampano,
    Lenguado o Rodaballo, Dorado, o Bonito). Puede ser una parte de un
    pescado más grande, pero uno más chico ni te molestes.

[^2]: A Ale le gustan, pero la verdad es un pedo dejarlas. Parecen
    alfileres y practicamente no se les puede sacar nada

[^3]: Que la mano soporte 2-3 segundos solamente. De preferencia usar
    leña
