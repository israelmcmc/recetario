Salsa roja taquera
==================


## Ingredientes

-   4 tomates

-   ~3 dientes de ajo

-   ~20 chiles de árbol

-   ~1 cucharadita de sal

-   ~1/8 de cebolla

-   Aceite vegetal


## Instrucciones

1.  Asar los tomates en un sarten.

2.  Dorar los dientes de ajo y los chiles en aceite. Se calienta el
    aceite, se apaga la flama y después se echan.

3.  Licuar todo (sin el aceite). Usar pulsaciones para que quede como
    martajado.

4.  Hervir por unos minutos para que quede más espesa.
