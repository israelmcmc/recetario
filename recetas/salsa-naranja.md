Salsa naranja
=============

Basada en: [Salsa de Chile de árbol "secreto de los Taqueros" (Toque y
Sazón)](https://www.youtube.com/watch?v=k9JAnBb19W8)

## Ingredientes

-   4 jitomates

-   1 cebolla pequeña

-   ~1/3 taza de aceite

-   4 dientes de ajo

-   1 puño de chiles de arbol (~15 gr)

-   1 cucharadita de sal

-   1/2 cucharadita de pimienta


## Instrucciones

1.  Acitronar la cebolla a fuego medio en un poco del aceite.

2.  Agregar los ajos apachurrados y guisar por ~1 min.

3.  Partir los jitomates en 4 y esperar a que estén bien cocidos ( 5 -
    10 min). Que no queden con mucha agua pero que tampoco se sequen.

4.  Agregar los chiles y guisar por ~2 min

5.  Licuar con la sal y la pimienta hasta que este homogenea.

6.  Con la licuadora funcionando, agregar aceite en forma de hilo hasta
    que este cremosa y color naranja.
