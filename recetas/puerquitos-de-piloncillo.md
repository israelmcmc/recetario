Puerquitos de piloncillo
========================


## Ingredientes

-   500 gr de harina blanca (~3 2/3 de taza)

-   250 gr de piloncillo

-   1 1/2 taza de agua (360 ml)

-   3 clavos de olor

-   2 huevos a temperatura ambiente

-   1/2 taza de manteca vegetal (~75 gr)

-   1 cucharada de polvo para hornear (~9 gr)

-   1 cucharadita de bicarbonato de sodio (~6 gr)

-   1 cucharadita de levadura instantánea (~3 gr)

-   1 pizca de sal molida

-   1 cucharadita de aúzcar blanca

-   1/4 de cucharadita de canela en polvo

-   3 cucharadas de agua (~30 ml)

-   1 cucharadita de extracto de vainilla

-   1 molde en forma de puerquito


## Instrucciones

1.  Hacer un jarabe. Mezclar la taza y media de agua con el piloncillo,
    el clavito de holor y la canela en polvo y ponerlo a hervier a fuego
    alto y mezclar por 15 minutos. Dejar a enfriar.

2.  Poner a fermentar la levadura. Disolver la levadura , la cucharadita
    de azucar y 1 cucharada de harina en 3 cucharadas de agua tibia.
    Dejar reposar hasta que duplique su tamaño.

3.  Mezclar la harina con el bicarbonato y el polvo para hornear.

4.  Romper los huevos en recipientes separados. Uno es para barnizar y
    el otro es para la masa. Batir el huevo para barnizar.

5.  Una vez que se enfríe el jarabe colarlo. Deben salir aproximadamente
    1 1/4 de taza o 300 ml

6.  Mezclar la harina con la manteca vegetal hasta que esté bien
    integrada.

7.  Agregar el resto de los ingredientes: huevo, jarabe, sal y levadura.
    Y amasar aproximadamente por 5 minutos. La masa es bastante
    aguadita, es normal, no agregar mucha más harina.

8.  Poner la masa en un recipiente tapado y dejar reposar en el refri
    por 10 minutos.

9.  Engrasar y enharinar 2 charolas

10. Extender la masa en hasta que quede de aproximadamente 2 cm de
    grosor. Usar harina para que no se pegue.

11. Cortar cochinitos.

12. Retirar el exceso de masa y colocar los puerquitos en charolas
    engrasadas y enharinadas.

13. Incorporar el exceso de masa en una bolita (con cuidado de no amasar
    mucho) y volver a extender para cortar más puerquitos.

14. Barnizar los puerquitos con el huevo batido

15. Hornear a 180 °C o 350°F hasta que estén doraditos.
