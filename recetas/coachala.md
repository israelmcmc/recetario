Coachala
========

Fuente: recetario abue

## Ingredientes

-   1 pollo entero [^1]

-   kilo de masa

-   100 gr de chile ancho (sin semilla)

-   100 gr de manteca

-   kg de tomate verde

-   6 dientes de ajo

-   Sal al gusto (~2-4 cucharaditas)


## Instrucciones

**Pollo**

1.  Cocer el pollo(~20 en olla a presión). Reservar el caldo.

2.  Desmunzar la pechuga, piernas, alas y muslos.

3.  Licuar el resto del pollo (¡sin los huesos!) con un poco del caldo.

**Guiso**

1.  Dorar los chiles por unos segundos en la manteca. Reservar la
    manteca requemada.

2.  Cocer lo tomates hasta que revienten.

3.  Licuar los tomates, el ajo, los chiles y un poco de caldo.

4.  Colar la salsa

5.  Añadir agua/caldo a la masa para hacer un atole y llevar a hervor,
    meneando todo el tiempo.

6.  Añadir a la salsa la manteca requemada, la sal y la salsa, y llevar
    a hervor, meneando todo el tiempo.

**Final**

1.  En una olla aparte vaciar el pollo desmenuzado, el pollo molido y el
    guiso. Dependiendo del tamaño del pollo quizá no todo el guiso sea
    necesario.

2.  Dejar hervir, meneando, hasta que adquiera la consistencia adecuada.
    Si es necesario agregar más caldo/agua.

3.  Servir con tostadas y cebolla desflemada con limón.

[^1]: Originalmente se hacía con una gallina
