Camarones a la diabla
=====================

Fuente: [Vicky Receta Fácil - Camarones a la
diabla](https://www.youtube.com/watch?v=gDJC0GQnj7A)

## Ingredientes

-   kg de camarones

-   3 dientes de ajo

-   2 limones

-   2 jitomates

-   de cebolla

-   3 chiles guajillos sin semilla

-   Chiles de árbol la gusto

-   1 chile chipotle de lata

-   1 cubo de caldo de pollo

-   2 cucharadas de matequilla

-   1 cuchara de aceite de oliva

-   Sal al gusto

-   Pimienta al gusto


## Instrucciones

1.  Cocer los tomates, los chiles de árbol y guajillo.

2.  Licuar los chiles con la cebolla, un diente de ajo, el limon, el
    caldo de pollo y un poco de agua.

3.  Colar la salsa

4.  Derretir la mantequilla a fuego bajo

5.  Añadir el aceite

6.  Agregar dos ajos triturados y cocer por unos segundos

7.  Agregar los camarones y cocer mezclando a fuego bajo hasta que estén
    rosas.

8.  Agregar pimienta y sal

9.  Agregar la salsa y dejar hervir por 1min.
