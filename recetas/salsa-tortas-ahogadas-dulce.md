Salsa tortas ahoagadas de tomate
================================


## Ingredientes

-   1 kg de jitomate

-   cucharaditas de comino

-   2 cucharaditas de oregano

-   3 dientes de ajo

-   de cebolla (~25 gr)

-   cucharaditas de pimienta molina

-   1 tsps de sal

[Preparación]{.underline}

1.  Cocer los tomates hasta que revienten.

2.  Licuar todo.

3.  Colar
