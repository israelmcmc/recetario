Salsa de chile puya
===================

Basada en: [Mexican authentic recipes - Salsa de chile
Puya](http://www.mexican-authentic-recipes.com/salsa_y_dips-salsa_chile_puya.html)

## Ingredientes

-   4 tomates

-   1 jitomate

-   4 chiles puya

-   2 chiles guajillos

-   ~10 chiles de arbol

-   2 dientes de ajo

-   ~1 cucharadita de sal


## Instrucciones

1.  Retirar las semillas del piua y los guajillos

2.  Asar los chiles, el jitomate y los tomates.

3.  Poner los chiles en agua hirviendo unos minutos a que se ablanden.

4.  Licuar todo.
