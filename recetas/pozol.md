Pozol
=====

Basada en: [Brisa Colibrí -
Pozol](https://www.youtube.com/watch?v=YW4xRdy0nHs)


## Ingredientes

-   250 gr de maíz

-   100 gr de semillas de cacao

-   1 cucharada de cal + 1 1/2 ml de agua

-   ~2 L de agua


## Instrucciones

1.  Nixtamalizar el maiz. Cocer por 2 hr.

2.  Cambiar el agua varias veces ( ~4) hasta que salga el agua
    clara.

3.  Tostar los granos de cacao de fuego bajo por ~20 min y pelar.

4.  Moler todo lo más fino posible. Quizá son necesarias dos pasadas.

5.  Disolver en agua, de preferencia con las manos.
