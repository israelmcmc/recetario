Quesillo de Oaxaca
==================

Basada en : [Queso Oaxaca Recipe - New England Cheese
Making](https://cheesemaking.com/products/queso-oaxaca-recipe), [SAGARPA
- Elaboración de quesos tipo Panela y
Oaxaca](http://www.sagarpa.gob.mx/desarrolloRural/Documents/fichasaapt/Elaboraci%C3%B3n%20de%20quesos.pdf)
y Los quesos mexicanos tradicionales, de Villegas, et al.

## Ingredientes

-   2 galones de leche [^1]

-   1/2 de pastilla para cuajar [^2]

-   20 - 30 gr de sal

-   Opcional: 1/2 cucharadita de inoculador termofílico [^3]

-   Opcional: 1 cucharadita de cloruro de calcio al 32% en peso [^4]


## Instrucciones

1.  Llevar la leche a 35 C ( 94 F)

1.  Acidificar la leche [^5]

    -   **Metodo tradicional**. Si se usa leche bronca simplemente se
        puede dejar reposar por 8 - 24 hr, dependiento de la temperatura
        del ambiente, hasta que alcance el nivel de acidez necesario.

    -   **Por inoculación**. Espolvorear el inoculador en la superficie
        y dejar que se humedezca por 2 min. Revolver. Reposar por ~
        3 hr[^6].

1.  Llevar la leche a 35 C ( 94 F) de nuevo.

1.  Diluir la pastilla de cuajo y agregar.

1.  Dejar reposar por 30 - 90 min, hasta que se pueda cortar con un
    cuchillo y salga limpio.

1.  Cortar cuadrados de ~1 pulgada. Dejar reposar por ~10
    min.

1.  Batir para moler los granos ligeramente. Dejar reposar por ~5
    min.

1.  Remover el suero con un colador, sin apretar.

1.  Dejar reposar por ~5 hr en un lugar no muy frío
    hasta que el suero se acidifique [^7] y pase la prueba de
    elasiticidad (un fragmento de cuajada se pone en agua a 80 C (
    175 F) por 5 - 10 min, si se estira ya es el punto).

1.  Rebanar el cuajo en tiras y poner en agua o suero a 80 C ( 175 F) por 5 - 10
    min.

1. Estirar el cuajo y doblarlo sobre sí mismo ~20 veces. Poner el
    queso en el agua caliente (que debe de mantenerse a ~80 C)
    después de cada ciertos dobleces, cuando ofrezca resistencia. Meter
    las manos en agua con hielo periodicamente para no quemarse.

1. Meter al agua caliente por última vez y comenzar a estirar desde un
    extremo formando una tira larga delgada, de preferencia plana[^8],
    que se va introduciendo en el agua fría con hielos conforme va
    saliendo.

1. Orear por ~30 min.

1. Frotar con sal. Aproximadamente 3% del peso final del queso.

1. Formar las bolas. Envolver en plástico y dejar reposar unas horas
    para que absorba la sal.

[^1]: De preferencia bronca. No necesita pasteurizarse, ya que durante
    el proceso se lleva la temperatura del cuajo a 80 C. Leche
    pasteurizada (en especial a bajas temperaturas, sin homogeneizar)
    también podría funcionar si se inocula primero la leche y se usa la
    cloruro de calcio. La leche ultra-pasteurizada convencional nunca
    jala.

[^2]: Esto es para la marca Walcoren.

[^3]: Esto es para el Thermo B Starter Culture. Seguir indicaciónes para
    otra marca. Se necesita el mismo tipo que para hacer mozzarella. Si
    la leche no esta pasteurizada, se puede usar el método tradicional y
    no se necesita, aunque es más rapido.

[^4]: Para leche pasteurizada, no se necesita para leche bronca.

[^5]: A 30 - 32 D. Los grados Dornic se pueden medir mediante
    titulación, pero no es necesario.

[^6]: En lugares fríos quizá sea necesario mantener la temperatura a
    ~35 C mediante baño María durante todo el proceso

[^7]: 32 - 35 D

[^8]: Una alternativa es ponerlos en una mesa y rociarlos con agua fría.
    Así quedan las tiras quedan planas como un liston.
