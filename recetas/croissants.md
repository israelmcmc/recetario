Masa para croissants
====================

Basada en How to make bread por Emmanuel Hadjiandreou y [How To Make
Proper Croissants Completely By Hand por Joshua
Weissman](https://www.youtube.com/watch?v=hJxaVD6eAtc)


## Ingredientes

-   250 gr de harina

-   20 gr de azucar

-   1 cucharadita de sal

-   11/2 cucharaditas de levadura

-   125ml de agua tibia

-   150 gr de mantequilla


## Instrucciones

1.  Disolver la levadura en el agua tibia.

2.  Mezclar la harina, la sal y el azúcar. Sólo hasta que este
    homogénea, no amasar en este paso.

3.  Dejar reposar tapada en el refrigerador por 10 min y amasar jalando
    una parte de masa y empujándola en medio, 8 veces por todo el
    perimetro (ver Fig. [1](#fig:amasado-croissant){reference-type="ref"
    reference="fig:amasado-croissant"}

4.  Repetir el paso anterior, reposo y amasado, 3 veces más (un total de
    4 veces).

5.  Con la ayuda de papel encerado hacer un cuadrado con la masa de
    ~15x15cm, y dejar por ~8hrs en el refrigerador.

6.  Con la ayuda de papel encerado formar un rectángulo de mantequilla
    homogéneo de ~10x10cm.

7.  Envolver la mantequilla con la masa como se muestra en la Fig.
    [2](#fig:envoltura-croissant){reference-type="ref"
    reference="fig:envoltura-croissant"} y dejarla reposar envuelta 1hr
    en el refrigerador.

8.  Estirar un un rodillo como se muestra en la Fig.
    [3](#fig:doblado-croissant){reference-type="ref"
    reference="fig:doblado-croissant"}, y doblar en tres. Trabajar sobre
    una superficie enharinada. Dejar reposar envuelta en el refrigerador
    por lo menos 30 min (más si se vuelve difícil de estirar).

9.  Repetir el paso anterior 2 veces más (3 en total), girando la masa
    90deg cada vez.

10. Dejar reposar envuelta en el refri por lo menos una hora antes de
    usar.

![Así se amasa la masa, 4 de estas! La de la imagen esta amarilla porque le añadieron huevo.](../figures/amasado-croissant.png){#fig:amasado-croissant width="100%"}

![Así se envuelve la mantequilla! A mí me gustan más los cuadrados\...](../figures/envoltura-croissant.png){#fig:envoltura-croissant width="100%"}

![3 de estas!](../figures/doblado-croissant.png){#fig:doblado-croissant width="100%"}
