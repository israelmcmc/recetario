Paella marinera
===============

Basada en: [La biblia de la
paella](https://www.la-biblia-de-la-paella.es/recette/paella-de-marisco/)
y Chef Amadeo en el canal Paellas y Cocina Valenciana ([paella de
mariscos](https://www.youtube.com/watch?v=WbaWsTqg9CY), [fumet de
pescado](https://www.youtube.com/watch?v=rem5NKXjEBo))
Sobre la paella (el sartén): es importante que la altura del arróz no
sea de más de 2 - 3 cm. Se necesita un sartén tal que tenga un area de 3
- 4 cm<sup>2</sup> por gramo de arroz, y que el fuego sea uniforme en toda la
base. Esta receta funciona bien para dos personas usando un sartén
normal de ~30 cm sobre una ornilla normal grande. Si se quiere
escalar se necesitaría una paella de verdad con un horno paellero (o
usar leña).

## Ingredientes

**Fumet (caldo o fondo de pescado)**

-   2 litros de agua

-   700 gr de moralla (e.g. cangrejo, pescado y camarones[^1]

-   1 poro

-   1 cebolla pequeña

-   1 zanahoria

-   1 bara de apio

-   1 chile ñora[^2]

-   1 diente de ajo

-   Una ramita de perejil

-   Aceite de oliva

**Paella**

-   1 taza de arroz bomba[^3] ( 200 gr)

-   400 gr de mariscos (e.g. pescado y gambas)

-   3 tazas de fumet

-   20 hebras de azafrán

-   1/2 taza de agua

-   1/2 cucharadita de pimentón dulce en polvo (aka paprika)

-   1/4 cebolla

-   1/2 tomate

-   2 1/2 cucharadas de aceite de oliva

-   1 diente de ajo

-   1 cucharadita de sal


## Instrucciones

**Fumet**

1.  Partir todas las verduras en trozos grandes y sofreir en suficiente
    aceite con el chile ñora y el ajo machacado.

2.  Sofreir la moralla.

3.  Si se usan cangrejo o similares, machacar un poco para romper el
    caparazón.

4.  Poner el agua a fuego alto, y cuando esté hirviendo agregar todos
    los ingredientes

5.  Bajar el fuego y cocer por 20 min más.

6.  Colar

**Paella**

1.  Triturar el azafrán con un mortero y poner en la 1/2 taza de agua
    por 30 min

2.  Picar la cebolla y poner a sofreir a fuego medio bajo en el aceite
    por ~2 min.

3.  Rallar el jitomate y agregar. Sofreir por 1 min.

4.  Agregar el pimentón y mezclar.

5.  Si se usan camarones o similares, cocer en el sofrito por ~
    30 - 60 s por lado. Sacar y reservar.

6.  Agregar los trozos de pescado y cocer ligeramente.

7.  Agregar el fumet y la sal. Subir a fuego alto y esperar a que se
    halla evaporado ~1/2 taza de agua (~2 min si el fumet ya
    estaba caliente).

8.  Agregar el arroz y el agua con el azafrán.

9.  El tiempo de cocción total del arroz es de 18 min, de la manera
    siguiente:

    1.  A fuego alto los primeros 8 min. El agua llega mas o menos a la
        altura del arroz.

    2.  Si se usaron camarones o similares, en este punto se ponen sobre
        el arroz, presionando ligeramente.

    3.  A fuego bajo por los siguientes 8 min. El agua se ha evaporado
        casi por completo, sólo queda un capa fina en el fondo.

    4.  A fuego alto los últimos 2 min.

    5.  Durante los últimos 30 - 60 s se debe de escuchar que se está
        dorando el arroz del fondo (*socarrat*). Debe de quedar café
        oscuro mas no quemado. Revisar con una cuchara que se haya
        formado socarrat en todas partes (la cuchara no patina en el
        fondo).

10. Dejar reposar por un par de minutos antes de servir.

[^1]: Como cangrejos, pescados pequeños, cabezas o esqueletos de
    pescado, cabezas de camarón, etc., pero nunca víseras.

[^2]: Me parece que es el mismo que el bola o cascabel de México, pero
    no estoy seguro

[^3]: El arróz bomba absorbe mucha agua y sabores, y al terminar la
    cocción queda firme y con los granos separados. Puede que otro arroz
    corto sirva, pero quizá la proporción de agua y el tiempo de cocción
    sean diferentes.
