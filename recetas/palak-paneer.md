Palak Paneer
============

Basada en : [Palak paneer - Vah Reh
Vah](https://www.vahrehvah.com/palak-paneer) y [Ranveer Brar](https://www.youtube.com/watch?v=5lVLxEr_qgM&ab_channel=ChefRanveerBrar)

## Ingredientes

-   250gr de espinacas

- 1 hoja de laurel

- 1 pizca de asafetida

- 4 clavos de olor

- 2 gotas de kewra

-   1 de cebolla chica picada

-   2 chile verdes

-   1 cucharadita de chile en polvo

-   1 cucharada de pasta de gengibre y ajo

-   1 tomate picado

-   Sal al gusto

- 1 cucharada de crema

-   1 cucharadita de semilla de cilantro molido

-   1/2 cucharadita de comino molido

-   1/2 cucharadita de garam masala

-   2 cucharaditas de alholva (fenogreco)

-   1/2 de cucharaditas de curcuma (turmeric)

-   150 gr de paneer en pedacitos de ~1cm

-   2 cucharadas de aceite (o ghee)


## Instrucciones

1.  Lavar las espinacas y ponerla en agua hirviendo por dos minutos.
    Desechar agua.

1.  Licuar la espinaca con agua y reservar.

1.  Calentar el aceite a fuego bajo y sofreir el clavo, la hora de laurel y la asafetida. Si se usa garam
    masala entera agregarla en este paso.

1.  Agregar la cebolla y un poco de sal. Freir hasta que este café.

1. Agregar el ajo con gengibre, y el chile. Sofreir por unos minutos.

1.  Agregar el tomate y freir hasta que se haga pasta.

1.  Agregar todas las especias, excepto el garam masala (si es molido).

1.  Agregar la espinaca molida y dejar hervir hasta que tenga unas
    consistencia cremosa.

1. Agregar la crema.

1.  Agregar el paneer, garam masala (si es molido) y kewar. Dejar por unos minutos.
