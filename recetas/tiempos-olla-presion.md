Olla de presión
===============

                         Tiempo (min)  Notas
  --------------------- -------------- ----------------------------------------
  Chayote con espinas         6        
  Lengua                      50       
  Lomo                      15-20      20 min para desmenuzar
  Bistec                    15-20      Grosor 3-15 cm
  Frijoles                  ~20.       Sin remojar.
  Garbanzo                  ~20        Sin remojar.