# Pollo frito de Ankit

Kodi Vepudo estilo Andhra Pradesh. Receta de Ankit Etha. 

## Ingredientes
- 1 kg de pollo 
- 3 cucharadas de aceite
- 2 hojas de laurel
- 4 cebollas medianas
- 3 chiles verdes picados [^1]
- 10-15 hojas de curry
- 1.5 cucharadas de pasta de ajo y gengibre 
- 1 cucharadita de cúrcuma (tuermeric)
- 1 cucharada de garam masala
- 1.5 cucharadas de chile en polvo [^2]
- 3 cucharadas de masala para pollo [^3]
- 3/4 - 1 cucharadita de sal.
- Cilantro para servir

## Instrucciones
1. Guisar las hojas de laurel en el aceite por ~1 min 
1. Guisar la cebolla con la olla tapada a fuego bajo hasta que se caramelizen. Menear regularmente.
1. Agregar el pollo en pedazos chicos. Olla tapada, fuego medio, menear regularmente. Esperar hasta que se haya evaporado la mayoría del agua que soltó el pollo (~30 min).
1. Subir a fuego medio y no dejar de menear. A partir de aquí, agregar un poco de agua si se está secando mucho para evitar que se peque.
1. Remover la tapa y agregar la paste de ajo y gengibre, y la sal. Guisar por un par de minutos.
1. Agregar la cúrcuma, el chile en polvo y la mezcla de especias para pollo. Guisar por unos minutos hasta que "salga el aceite" (no luzca seco).
1. Agregar los chiles verdes y las hojas de curry. Guisar por unos minutos.
1. Esparcir el garam masala y guisar moviendo por 1 min.
1. Al final todo debe de quedar casi seco. Servir con cilantro.


[^1]: Delgados, parecidos al chile de árbol verde.

[^2]: e.g. MDH Deggi Mirch "chilli powder for curries"

[^3]: e.g. MDH Chicken masala. Contiene: Coriander seeds, Red Chillies, Turmeric, Cumin, Iodised Salt, Black Pepper, Fenugreek Leaves, Mustard, Dried Ginger, Cassia, Cardamom Amomum, Cloves, Nutmeg, Mace, Asafoetida.

