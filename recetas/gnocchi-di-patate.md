Gnocchi di patate
=================

Basado en [Gnocchi di patate en Giallo
Zafferano](https://ricette.giallozafferano.it/Gnocchi-di-patate.html)


## Ingredientes

-   300 gr de harina

-   1 kg de papa grande (i.e. russet)

-   1 huevo

-   Una pizca de sal

**Intrucciones**

1.  Poner las papas en agua hirviendo y cocer hasta que un tenedor entre
    fácilmente ( ~30 - 45 min).

2.  Pelar las papas mientras aún estan calientes.

3.  Pasar por una prensa de papas[^1]. Debería de quedar 900 gr de puré.

4.  Mientras el puré de papas ún esta caliente, mezclar con la harina,
    el huevo y la sal.

5.  Amasar hasta que esté homogéneo, y no más. No debería de tardar más
    de 5 min.

6.  En una superficie enharinada con sémola[^2], rodar una porción de
    masa y hacer una tira alargada de ~1.5 cm de diámetro (más o
    menos el grosor de un dedo)

7.  Cortar en trozos de ~1.5 cm de ancho.

8.  Rodar por una tablilla estriada[^3] enharinada con sémola, usando el
    pulgar para aplastarlos libgeramente y dejar una muesca del lado de
    abajo. Se deben de rodar de forma transversal a como se redó la masa
    al formar la tira. Ver Fig

9.  Poner en agua hirviendo con sal[^4][^5] hasta que suban a la
    superficie (~1 min).

![Así se le dan forma a los gnochis. Es importante que el rodaje sea transversal al corte, es decir, que la parte "pegajosa" pase por la tablilla,. De lo contrario los borden no quedan redondos y los zurcos no son tan pronunciados.](../figures/rodado-gnocchi.png){#fig:rodado-gnocchi.pdf width="\\textwidth"}

**Notas**
Si no se van a cocer y comer apenas hechos, para guardar los gnocchis
hay que congelarlos de forma individual en charolas. Para cocerlos se
echan al agua hirviendo directamente sin descongelar. Es importante usar
mucha agua y sólo cocer de pocos a la vez.

[^1]: O aplastas las papas y pasar por colador.

[^2]: La sémola es mejor que la harina para que no se pegue a los
    gnocchis. No confundir con semolina, que es de grano más grueso.

[^3]: O un tenedor

[^4]: Es importante que la razon entre gnocchis y agua sea grande, para
    que no baje la temperatura del agua. Aún mas importante que para
    pasta normal.

[^5]: 2 cucharaditas de sal por litro de agua
