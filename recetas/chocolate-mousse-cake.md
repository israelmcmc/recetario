Chocolate Mousse Cake
=====================

Fuente: [Life, love and sugar - Chocolate Mousse
Cake](https://www.lifeloveandsugar.com/2017/10/23/chocolate-mousse-cake/)

## Ingredientes

**Pan**

-   230gr de harina

-   420 gr de azúcar

-   85 gr de cocoa

-   2 1/4 cucharaditas de bicarbonato de sodio

-   1/2 cucharaditas de polvo para hornear

-   1 cucharadita de sal

-   1 taza de leche

-   1/2 tazas de aceite vegetal

-   1 1/2 cucharaditas de vainilla

-   2 huevos

-   1 taza de agua caliente

-   Mantequilla para el molde

**Mousse**

-   4 yemas de huevo

-   1/4 tazas de azúcar

-   1 3/4 tazas de heavy whipping cream

-   225gr de chocolate en barra

-   85 gr de azúcar glass

**Betun**

-   1 1/4 tazas de heavy whipping cream

-   35 gr de azúcar glass

-   30 gr de cocoa

-   1/2 cucharadita de vainilla


## Instrucciones

**Pan**

1.  Combinar ingredientes secos, luego líquidos, excepto el agua.
    Mezclar el agua al final.

2.  Dividir en tres moldes redondos. Estos deben de tener matenquilla en
    el fondo y una capa de papel encerado.

3.  Hornear a 350 F (180 C) por 20-25min, hasta que pase la prueba del
    palillo.

**Mousse**

1.  Mezclar las yemas, el azúcar y media taza de whipping cream. Cocinar
    a baño maría por ~10-15min batiendo continuamente hasta que
    espese y adquiera volumen. El agua debe de mantenerse a ~70C.

2.  Derretir el chocolate a baño maría o en microondas.

3.  Añadir a la primera mezcla y batir hasta que esté suave. Dejar
    enfríar.

4.  Batir el resto de la whipping cream (11/4 de taza) con la
    azúcar glass hasta que forme picos.

5.  Mezclar todo con movimiento envolventes, agregando de a poco a la
    vez.

**Betún**

1.  Bartir todo hasta que forme picos

**General**

1.  Hacer el pan y dejar enfriar.

2.  Hacer el mousse.

3.  Con la ayuda de un \"cake collar\" poner el mousse en medio de los
    panes. Dejar en el refrigerador por ~5 horas.

4.  Hacer el betún y poner una capa arriba.

5.  Guardar en refrigerador.
