Masa de maseca
==============


## Ingredientes ( ~4 tortillas)

-   ~1/2 taza de maseca (maíz amarillo es el mejor)

-   1/2 taza de agua


## Instrucciones

1.  Mezclar maseca y agua

2.  Si es necesario, agregar un poco de maseca. Debe de quedar lo más
    humeda posible pero que siga siendo manejable. La cantidad depende
    del tipo de maseca.

3.  Opcional: dejar reposar cubierta unos minutos. Esto hidrata la masa,
    la hace más manejable y resulta en tortillas que se inflan más
    fácilmente y son menos quebradizas.
