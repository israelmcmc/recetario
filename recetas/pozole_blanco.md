# Pozole blanco

Basado en [Pozole blanco - Vicki Receta fácil](https://www.youtube.com/watch?v=SrghIq1ZykM&ab_channel=VICKYRECETAFACIL)

## Ingredientes

### Caldo
- 2 kg de carne de puerco. Incluir espinazo o algo que tenga hueso. Cortados en pedazos medianos (~8-10 cm)
- 1/2 cebolla
- 1/2 cabeza de ajo 
- 2 hojas de laurel
- 2-4 cdas de sal (al gusto)

### Nixtamal
- 600 gr de maiz cacahuazintle
- 1 1/2 cdas de cal 
- 3 L de agua

### Recaudo
- 1 cda manteca
- 3 dientes de ajo
- 1/2 cebolla
- 4 clavos molidos
- 5 pimientas molidas
- 1 cda oregano o mejorana
- 1/2 cdita comino
- 1/4 cdita tomillo

### Acompañamientos
- Tostadas
- Limones
- [Salsa roja de antojitos](salsa-roja-taquera.md)
- Lechuga
- Orégano o mejorana
- Rábanos

## Instrucciones

### Nixtamal
1. Mezclar el agua, cal y maiz.
1. Calendar y hervir por 20 min
1. Deja enfríar (4-8 hr)
1. Lavar bien (~4 cambios de agua)

### Recaudo
1. Acitronar la cebolla y el ajo
1. Agregar las especias y cocer por 2-3 min. 

### Pozole
1. Agregar agua hasta que tape la carne
1. Agregar la cebolla, ajo (cortado) y hojas de laurel. De preferencia en una bolsita de tela para que sea fácil sacarlas.
1. Calentar y hervir por 30 min.
1. Remover la capa espumosa que se forma en la parte de arriba.
1. Agregar el nixtamal, y cocer por 1.5 horas más. Menear ocasionalmente. 
1. Remover la bolsita de tela, y agregar la sal y el recaudo.
1. Cocer por 1/2 hora más.
1. Menear antes de servir, pues se asienta una capa que parece atole.



