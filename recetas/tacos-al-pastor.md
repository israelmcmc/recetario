Tacos al pastor
===============

Basada en: [Luisito informa - Un día con un
taquero](https://www.youtube.com/watch?v=W03EIyBCOPI&list=LL70vwP7i8PV9gx3t6SvBbRQ&index=598),
[Al estilo del Chef Roger - Carne al
pastor](https://www.youtube.com/watch?v=F7N3l6XGn-0), [Toque y sazón -
Tacos al pastor](https://www.youtube.com/watch?v=pmzl92R3rJk), [Cultura colectiva - ¿Cómo se HACE un TROMPO AL PASTOR?](https://www.youtube.com/watch?v=XLSQWGIiF4s)

## Ingredientes

-   10 kg de cabeza de lomo de cerdo [^1]

-   150-200 tortillas (~6-8 kg de masa)

-   35 chiles guajillo

-   20 chiles anchos

-   5 chiles chipotles (sin adobar)

-   1 taza de jugo de naranja agria [^2]

-   1 1/2 taza de vinagre

-   10 dientes de ajo

-   20 clavos de olor

-   1 1/4 cucharaditas de comino

-   1 1/4 cucharaditas de oregano

-   5 cucharaditas de sal

-   20 pimientas gordas

-   60 gr de paste de achiote

-   Sal extra para la carne [^3]

-   1 cebolla grande para la base

-   1 piña para coronar


## Instrucciones

-   Quitar semillas y venas de los chiles secos.

-   Poner en agua hirviendo por ~5 min para que se ablanden.

-   Licuar con el resto de lo incredientes del adobo

-   Colar el adobo y descartar resto.

-   Cortar la carne en filetes de medio dedo de ancho (~7 mm).

-   Espolvorear cada filete con sal por ambos lados

-   Revolver con el adobo. Dejar marinar por la noche.

[Notas]{.underline}

-   Para armar el trompo se compienza con la base tapa de la piña y una
    cebolla grande para que el cuchillo no toque el metal. Después de
    ensartar los filetes las orillas de los filetes se doblan hacia
    andentro para darle forma. Intercalar filetes grandes con pedazos
    chicos. Conforme va creciendo se le van recortanto los pedazos
    sobrantes y dando forma con el cuchillo. Estos se intercalan entre
    pesazos más grandes en la capas siguiente (o se van para los
    alambres!).

-   Antes de empezar, rotar por 15 - 30 min en el fuego hasta que la
    última capa ya esté cocida y terminar de darle forma al trompo.

-   El cuello de botella es que tan rápido se dora el exterior del
    trompo. Por esto, usar la mayor cantidad de carbón posible y hacer
    el trompo lo más alto posible para aprovechar toda la columna de
    carbón.

-   Es crucial tener un cuchillo afilado y grande. Para el corte el
    cuchilo entra casi vertical, con tajadas en una sola dirección hacia
    uno, y tratando de que el corte no sea hacia los lados para no girar
    el trompo. El trompo bloquea el fuego de las manos para no quemarse.
    El corte debe de ser lo más delgado posible y salir laminado.

[^1]: En inglés se llama "Boston butt", aunque este incluye también una
    parte del omóplato y carne alrededor.

[^2]: Sustituir con jugo de naranja y 3 limones

[^3]: Debería de medir cuánta se usa para así revolverla junto con el adobo y hacerlo más fácil.
