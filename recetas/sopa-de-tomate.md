Sopa de tomate
==============

Basada en: [Tomato soup from fresh tomatoes - happy
hooligans](https://happyhooligans.ca/best-homemade-tomato-soup-recipe/).

## Ingredientes

-   3 cucharadas de mantequilla

-   2 cucharadas de aceite de oliva

-   1 cebolla

-   10 tomates

-   3 dientes de ajo

-   ~1/4 de cucharadita de sal

-   10 ramitas de tomillo fresco

-   1 ramita de romero

-   25 horas de albahaca.

-   1 cucharada de arroz


## Instrucciones

1.  Calentar la mantequilla y el aceite a fuego medio.

2.  Agregar la cebolla en tiras, la sal, el tomillo, el romero y el ajo.

3.  Cocinar tapado a fuego medio por ~30 min, hasta que la cebolla
    este suave. Menear ocasionalmente.

4.  Agregar el tomate partido en trozos, el arroz y las hojas albahaca.

5.  Cocinar destapado a fuego medio por ~30 min, hasta que alcance
    que la mayor parte del agua se haya evaporado.

6.  Remover los palitos de las ramitas de tomillo.

7.  Licuar a alta velocidad hasta que este homogeneo.
