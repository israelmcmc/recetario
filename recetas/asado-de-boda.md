Asado de Boda
=============

[Ingredientes (para taquiza)]{.underline}

-   2 naranjas

-   1 kg de lomo o pierna de cerdo (en cubitos)

-   ~5 hojas de laurel seco

-   ~4 clavos

-   ~4 palitos de canela entera

-   sal y pimienta

-   ~2 chiles pasilla secos

-   ~2 chiles guajillo secos

-   ~3 chiles piquines secos

-   ~3 chiles de arbol secos

-   ~2 chiles anchos secos

-   1/2 cebolla

-   ~5 dientes de ajo


## Instrucciones

1.  Curtir la carne de cerdo en el jugo de las dos naranjas, el clavo,
    la canela, sal, pimienta, laurel y agregar la cascara de una naranja
    en rajitas (no usar la cascara de las dos para no amargar mucho la
    mezcla). Cocinar en slow cooker por ~4 - 5 hrs. en high.

2.  Remojar los chiles en agua caliente hasta que estén suaves.

3.  Cuando la carne este lista, separar los cubitos a un lado y colar el
    jugo que resulta (mezcla de jugo de naranja, jugo de carne y
    manteca). Enseguida licuar jugo junto con los chiles, cebolla y ajo.

4.  Colar salsa y en una cacerola calentar a fuego lento junto con la
    carne. Se puede remoler lo que queda en el colador con un poco de
    salsa y agua para aprovechar los ingredientes al máximo. Se guisa
    hasta por una hora a fuego muy lento.
