Galletas de mantequilla
=======================


## Ingredientes

-   150 gr de mantequilla

-   125 gr de azucar

-   1 huevo

-   175 gr de harina

-   1 cucharadita de canela molida

-   Pizca de sal


## Instrucciones

1.  Dejar la mantequilla afuera del refrigerador hasta que se suavice.

2.  Mezclar la mantequilla con el azúcar hasta que este cremoso.

3.  Agregar el huevo y mezclar.

4.  Agregar el resto y mezclar.

5.  Meter la mezcla cubierta con plastico al refrigerador por ~20
    min.

6.  En una superficie harinada extender la masa hasta que tenga 5-7mm de
    espesor.

7.  Cortar de la forma deseada y poner en un charola con papel encerado
    (o con mantequilla+harina).

8.  Volver a hacer bola el sobrante de masa y repetir.

9.  Hornear por ~20 min a 180°C hasta que las orillas esten
    cafes.
