Mole tabasqueño
===============

Receta ponderada de la Sra. Meraris y Sra. Leti (mamá y tía de Kris)


## Ingredientes

-   1 pollo

-   4 chiles de anchos

-   4 chiles pasilla

-   4 chiles mulato

-   1/2 de cebolla morada

-   3 jitomates chicos maduros

-   5 dientes ajos chicos

-   4 cucharada de ajonjolí

-   50 gr almendras [^1]

-   ~50 gr de pasitas [^2]

-   1/2 de cucharadita de oregano

-   6 clavos

-   6 pimientas gordas

-   50 gr de galletas María o de animalitos

-   Aceite

-   Sal (~4 cucharaditas)

-   110 gr de piloncillo


## Instrucciones

1.  Cocer el pollo por 1 hr y reservar el caldo

2.  Quitarle las semillas a los chiles, cocer por 5 min y escurrir.

3.  Cocer las almendras por 5 min y pelarlas

4.  Dorar el ajonjolí a fuego bajo hasta que empiecen a cambiar de color

5.  Freir las galletas hasta que empiecen a cambiar de color

6.  Sofreir la cebolla, luego el ajo y luego el tomate. El sofrito debe
    de quedar bien cocido hasta que se forme una "pasta".

7.  Moler la pimienta , oregano y clavo. Agregar al sofrito.

8.  Freir los chiles ligeramente, solo por unos cuantos segundo por
    lado.

9.  Licuar los chiles con caldo de pollo y piloncillo. Poner a hervir
    por 20 min.

10. Licuar todo junto. Agregar suficiente caldo de pollo para que quede
    aguadito.

11. Colar.

12. Agregar el pollo y 1/2 taza de aceite.

13. Hervir por 30 min, meneando ocacionalmente. Si está quedando muy
    espeso, agregar más caldo de pollo.

[^1]: La receta dice 20, creemos que más es mejor

[^2]: La receta decía 10 pesos (!), asi que hay que ver
