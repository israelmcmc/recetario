Crema pastelera
===============

Fuente: Creme Patissiere de Julia Child en The French Chef Cookbook


## Ingredientes

**Masa**

-   6 yemas de huevo

-   1/2 taza de azúcar

-   1/2 taza de harina

-   2 tazas de leche (menos para más espesa)

-   2 cucharadas de mantequilla

-   1 cucharada de vainilla

-   2 cucharas de ron o brandy


## Instrucciones

1.  Batir las yemas con el azúcar hasta que la mezcla este espesa y
    uniforme.

2.  Agregar la harina y batir.

3.  Calentar la leche, sin que llegue a hervir (~50 C).

4.  Agregar la leche poco a poco, batiendo.

5.  Poner a fuego medio[^1] y continuar batiendo constantemente. Cuidar
    de raspar el fondo y los lados para que no se pegue.

6.  Cuando se empiecen a formar grumos, bajar a fuego lento y seguir
    batiendo por varios minutos hasta que espese.

7.  Quitar del fuego y batir con la mantequilla, la vainilla y el ron.

8.  Dejar reposar. Poner un capa fina de leche en la superficie para que
    no se seque.

[^1]: Hacerlo en una olla pesada lo hace más facil
