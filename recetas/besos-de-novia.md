Besos de novia
==============

Basada en: [BESOS DE NOVIA - Fernanda Berlei
](https://www.youtube.com/watch?v=V18ECzDqUjM)
También llamados polvorones, bolitas de nuez, pedos de monja o galletas
de boda.


## Ingredientes

-   4oz de mantequilla sin sal (113gr, una barrita)

-   Azúcar glass

    -   45 gr para la masa

    -   Extra para espolvorear

-   150gr de harina de trigo

-   3/4 de cucharadita de canela en polvo

-   Nuez molida

    -   100gr bien molida (polvo).

    -   15gr en pedacitos


## Instrucciones

1.  Acremar la mantequilla

2.  Agregar el azucar y mezclar

3.  Agregar y mezclar la canela, harina y nuez.

4.  Hacer bolitas con una cuchara (~25gr) y poner en una bandeja
    encerada o con papel encerado.

5.  Hornear a 180 C (360F) por ~16min. Se deben de poder volter
    facilmente y el fondo debe de estas doradito, pero no café oscuro.

6.  Revolcar en azúcar glass.
