Estofado (receta de Elim)
=========================


## Ingredientes

-   500gr de pollo/puerco/res/tofu en cuadritos

-   2 papas medianas

-   2 zanahorias

-   1paguete con especias (*spice pounch*. e.g. Tomax: anis, comino,
    clavo, regaliz, canela, nuez moscada y laurel)

-   4 dientes de ajo finamente picados

-   4 cucharadas de aceite de sesamo

-   2 cebollines rebanados

-   4 rebanadas de gengibre finamente picadas

-   2 cucharada de pasta koreana (*Doenjang, korean bean past*. e.g.
    Chung Jung One Sunchang Ssamjang)

-   4 cucharadas de vino de cocinar japonés (*mirin*)

-   4 cucharadas de salsa de soya

-   Chile seco al gusto (1-2 cucharadas) (red chilli pepper flakes)


## Instrucciones

1.  Pelar y cortar en trozos pequeños (1-2 cm de lado) la papas y la
    zanahoria

2.  Hacer una salsa mezclando la pasta koreana, el vino, la salsa de
    soya y el chile seco.

3.  En un sartén grande calentar 2 cucharadas de aceite y echar el ajo y
    el gengibre.

4.  Apenas desprenda olor, antes de que se queme, agregar la papa, la
    zanahoria, la salsa, el paquete de especias y agua hasta que se
    cubra.

5.  Deja a fuego alto hasta que se evapore el agua y se obtenga una
    consistencia espesa. Remover el pequete de especias.

6.  Freir la carne o tofu con dos cucharadas de aceite de sesamo.

7.  Mezclar todo y poner los cebollines arriba.

8.  Acompañar con arroz blanco simple
