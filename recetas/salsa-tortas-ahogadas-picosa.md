Salsa tortas ahogadas picosa
============================


## Ingredientes

-   25 chiles de árbol secos

-   cucharaditas de pimienta

-   1 cucharada de vinagre de manzana

-   taza de agua (donde se cocieron los chiles)

-   cucharadita de sal

[Preparación]{.underline}

1.  Cocer los chiles secos (~5min en agua hirviendo)

2.  Licuar los chiles con todo y semilla, con una agua de donde se
    cocieron y el resto de los ingredientes.

3.  Colar

4.  Usualmente se le agrega medios aros de cebolla.
