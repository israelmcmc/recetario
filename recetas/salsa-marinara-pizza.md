Salsa pizza
===========


## Ingredientes

-   130gr (1/3 de taza) de pasta de tomate

-   360gr (1 1/2 de taza) de tomates enlatados drenados

-   2 cucharaditas de aceite de oliva

-   1 cucharadita de orégano

-   1 cucharadita de sal

-   1 cucharadita de ajo en polvo

-   Un manojo de albahaca (~15 gr).

## Instrucciones

1. Licuar todo.
