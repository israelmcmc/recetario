Pastel de tres leches
=====================

Basada en: [Pastel de tres leches - El mejor
nido](https://www.elmejornido.com/es/recetas/pastel-tres-leches-121630)

## Ingredientes

**Pan (por piso, hacer 3 pisos)**

-   3 claras de huevo

-   3 yemas de huevo

-   4 cucharadas de azúcar (uso dividido)

-   1/2 taza de harina

**Jugo**

-   1 lata (14oz) de leche condensada

-   1 lata (7.6 fl. oz.) de media crema o 1 taza de whipping cream

-   2/3 de taza de leche evaporada

-   1/4 de taza de brandy[^1]

-   1 cucharadita de vainilla

**Crema batida**

-   1 taza de whipping cream

-   2 cucharadas de azucar

-   1/2 cucharadita de vainilla

**Extras**

-   Mermelada (entre las capas)

-   Mantequilla y harina (para el molde)


## Instrucciones

**Pan**

1.  Batir las claras y 2 cucharadas de azúcar a punto de turrón

2.  Batir las yemas y 2 cucharadas de azúzar hasta que este homogéneo

3.  Incorporas los dos batidos y la harina con movimientos envolvientes
    suaves

4.  Hornear a 375 F (190 C) en un molde engrasado y enharinado hasta que
    pase la prueba dle palillo (~12 min para un molde redondo de
    9in).

**Jugo**

1.  Mezclar todo bien hasta que esté homogéneo

**Crema batida**

1.  Batir todo hasta que esté firme

**Todo**

1.  Poner una capa pan en donde se va a servir y con un tenedor o
    palillo agujerearla toda.

2.  Ir mojando con el jugo poco a poco dejando que se absorba, hasta que
    ya no pueda absorver más[^2].

3.  Poner un capa de mermelada

4.  Repetir con las otras dos capas.

5.  Forrar con crema batida.

[^1]: Probar con más, la última vez que lo hice no sabía mucho. Quizá
    fue por usar amaretto.

[^2]: Me parece que es mejor hacerlo mientras el man esta caliente para
    que se sature más fácilmente.
