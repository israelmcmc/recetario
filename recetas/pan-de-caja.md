Pan de caja
===========


## Ingredientes

-   500 gr de harina

-   2 cucharadita de sal

-   2 cucharadita de levadura

-   300 + 80 gr de agua

-   Aceite de oliva (para engrasar)


## Instrucciones

1.  Mezclar la levadura en 80ml de agua tibia.

2.  Mezclar con el resto de los ingredientes y amasar.

3.  Dejar reposar por ~1.5hrs (hasta que doble el tamaño).

4.  Poner en un molde de panque engrasado con aceite y esperar 1-2hrs a
    que duplique su tamaño.

5.  Hornear por ~35min a 200C (400F).
