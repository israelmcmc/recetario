Mango con sticky rice
=====================

Basada en: [Authentic Mango Sticky Rice Recipe - Mark
Wiens](https://www.youtube.com/watch?v=H_R108b6ZQg)

## Ingredientes

-   1 taza de arroz

-   400 ml de leche de coco [^1]

-   30 gr de azúcar ( 2 cucharadas)

-   1/8 de cucharadita de sal (2 pizcas)

-   2 mangos

-   Mung beans o ajonjolí tostado


## Instrucciones

1.  Dejar el arroz remojando por la noche[^2].

2.  Drenar el agua

3.  Envolver en una mante de cierlo y cocer al vapor por 15 min.

4.  Diluir el azúcar y la sal en la crema de coco y llevar a ebullición.

5.  Mezclar un poco de esta mezcla con el arroz, tal que quede
    "amasable".

6.  Seguir cociendo la crema de coco, moviendo regularmente, hasta que
    espese.

7.  Servir con mango, la crema de coco y opcionalmente con mung beans o
    ajonjolí.

[^1]: No usar crema de coso ya endulzada. Se puede hacer moliendo la
    carne de un coco con 3 - 4 tazas de agua y pasandola por una manta
    de cielo.

[^2]: O por lo menos 1 hr. Quizá necesita unos minutos mas de cocción.
