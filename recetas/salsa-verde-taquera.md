Salsa verde taquera
===================

Basada en: [Easy Recipes, \* Chef-roger style \* - como hacer salsa
verde, taquera, Receta \#104, como hacer salsa
verde](https://www.youtube.com/watch?v=sSBfWMCO508)

## Ingredientes

-   ~5 chiles verdes

-   1/2 kg de tomate verde

-   2 dientes de ajo

-   1 cebolla chica

-   1 manojo de cilantro

-   Sal al gusto (~1 cucharada)


## Instrucciones

1.  Cocer los tomates, los dientes de ajo y la mitad de la cebolla hasta
    que los tomates cambien de color.

2.  Licuar

3.  Picar el cilantro y el resto de la cebolla

4.  Añadir a la salsa junto con la sal.
