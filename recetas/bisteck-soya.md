Bisteck molido
==============


## Ingredientes

-   litro de agua

-   1 cucharada de oregano molido

-   1 cucharada de sal

-   1 cucharadita de ajo molido

-   1/4 de cebolla picada finamente

-   de cucharadita de pimienta

-   de cucharadita de comino molido

-   100 gr de soya texturizada

-   1 cucharada de aceite vegetal


## Instrucciones

1.  Se pone todo exepto la soya y el aceite en una cacerola hasta que
    hierva

2.  Se agrega la soya y se apaga la flama. Dejar reposar por 5 min.

3.  Con un colador y un cucharon exprimir la soya lo más que se pueda.

4.  Se fríe la soya preparada.
