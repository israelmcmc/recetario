Queso panela
============

Basada en : [SAGARPA - Elaboración de quesos tipo Panela y
Oaxaca](http://www.sagarpa.gob.mx/desarrolloRural/Documents/fichasaapt/Elaboraci%C3%B3n%20de%20quesos.pdf)

## Ingredientes

-   1 galón (3.8L) de leche pasteurizada [^1]

-   1/4 de pastilla para cuajar [^2]

-   30-40gr de sal

-   Opcional: 1/2 cucharadita de cloruro de calcio al 32% en
    peso [^3]


## Instrucciones

1.  Calentar la leche a ~32 ° C

2.  Opcional: Diluir el cloruro de calcio en 1/4 tazas de
    agua y agregar a la leche.

3.  Disolver la pastilla para cuajar en un poco de agua y revolver en la
    leche.

4.  Dejar reposar hasta que cuaje y se pueda cortar (30-60 min). El
    cuchillo sale limpio cuando esta lista..

5.  Hacer varios cortes especiados por un par de centimetros, en ambas
    direcciones.

6.  Dejar reposar por 15 min.

7.  Calentar muy lentamente hasta ~32 ° C mientras se
    menea por ~15 min para separar el suero.

8.  Dejar reposar por 15 min.

9.  Con la ayuda de un colador remover el suero[^4], pero sin exprimir.

10. Amasar la cuajada con la sal. Usar 3% del peso del cuajo.

11. Poner en un molde con estameña y exprimir suavemente el suero
    excedente.

12. Dejar reposar durante 15 min, voltear, y dejar reposar por 3 horas.

[^1]: De preferencia pasteurizada a bajas temperaturas y no
    homogeneizada. Quitar crema si no está homogeneizada. No usar leche
    ultra-pasteurizada.

[^2]: Puede variar dependiendo de la marca. Usé Walcoren que cuajaba
    hasta ~20litros por pastilla.

[^3]: Mejora el rendimiento, especialmente si la leche fue homogeneizada
    o pasteurizada a altas temperaturas.

[^4]: Has [queso ricotta](queso-ricotta.md) con el suero.
