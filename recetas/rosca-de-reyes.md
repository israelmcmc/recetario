Rosca de Reyes
==============

Basada en : [Vicky Receta Facil - Rosca de
Reyes](https://www.youtube.com/watch?v=4IgNDiX7cHg) y [Yuri de Gortari -
Rosca de reyes](https://www.youtube.com/watch?v=zVnEPQhfi-s)

## Ingredientes

**Masa**

-   135 gr de mantequilla

-   4 huevos

-   1/2 taza de leche tibia

-   2/3 de taza de azucar

-   1 cucharadita de sal

-   11 gr de levadura

-   2 cucharaditas de vainilla

-   1/2 kg de harina

**Costra**

-   100 gr de manteca vegetal

-   100 gr de harina

-   100 gr de azucar glass

-   2 yemas de huevo

**Extras**

-   Ate (e.g. mebrillo, guayaba, tecojote)

-   Acitrón

-   Higos cristalizados

-   Cerezas en almibar

-   Azucar

-   1 huevo

-   Monitos


## Instrucciones

1.  Poner la levadura en leche tibia, con una cucharada de azucar y una
    de harina. Dejar deposar hasta que las burbujas dupliquen el tamaño.

2.  Amasar el resto de los ingrediente de la masa hasta que despegue.

3.  Dejar reposar cubiertas hasta que duplique su tamaño (~1.5
    hr).

4.  Hacer una tira de 1m de largo.

5.  Meter monitos por abajo.

6.  Hacer la rosca en una charola engrasada o con papel encerado. Las
    puntas se unen envolviendo un lado con el otro.

7.  Mezclar los ingredientes de la costra hasta que este homogenea.

8.  Barnizar con una capa delgada de huevo

9.  Poner costra y dulces. Espolvorear azucar en la costra.

10. Dejarla reposar por ~30 min en un lugar caliente --arriba del
    horno por ejemplo--.

11. Hornear a 180C (350F) por ~20 min, hasta que se empiece a
    dorar la superficie, pero antes de que este café completamente.
