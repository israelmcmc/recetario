Pastel de zanahoria y betún
===========================


## Ingredientes (por capa)

-   1 taza de harina

-   1 taza de azucar

-   1/2 cucharadita de bicarbonato

-   1/2 cucharadita de levadura química

-   1/4 de cucharadita de sal

-   3/4 de taza de aceite vegetal

-   2 huevos

-   11/2 tazas de zanahoria rallada

-   1/4 de taza de nuez molida

-   1 cucharadita de vainilla

-   1/2 cucharadita de canela molida

-   1/4 de taza de pasitas


## Instrucciones

1.  Hacer una mezcla homogenea y hornear a 180° (350°F)
    hasta tan pronto como este firme (~20-30 min para cupcakes,
    ~50-60 min en un molde grande).

**Betún**


## Ingredientes

-   1 paquete de queso Philadelphia (8 oz)

-   1 barra de mantequilla (4 oz)

-   6 tazas de azucar glass


## Instrucciones

1.  Batir la mantequilla hasta que este cremosa.

2.  Agregar el queso y batir hasta que no haya grumos.

3.  Mezclar con la azúcar un poco a la vez, hasta que este homogéneo

4.  Refrigerar un tiempo para mejorar consistencia.
