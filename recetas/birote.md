# Birote

## Ingredientes

- 600 gr de harina
- 300 ml de agua tibia
- 120 gr de masa madre (100% hidratacion)
- 10 gr de sal

## Instrucciones

1. Mezclar la harina, el agua, y la masa madre. Amasar sólo hasta que esté homogéneo (2-3 min).
1. Dejar fermentar por ~10 hrs, dependiendo de la temperatura, hasta que doble su tamaño.
1. Partir en tres bolas de 360 gr cada una y poner sobre una superficie ligeramente enharinada.
1. Aplanar hasta tener un rectangulo de ~20x15 cm.
1. Doblar los dos extremos hacia el centro y aplanar con la mano.
1. Estirar un poco más.
1. Doblar un extremo a los 2/3 y sellar con la mano.
1. Doblar completamente y sellar.
1. Rodar para estirar. La forma de la piocha se le da rodandolo con el canto de la mano.
1. Poner en una manta de lino enharinada, con el doblez hacia abajo. Los birotes se acomodan haciendo "zurcos" con la manta.
1. Dejar fermentar por ~2 hrs hasta que se vean turgentes (un poco menos que doblar su tamaño). Si salen planos, para la otra dejar fermentar menos.
1. Calentar el horno a 250 C (480 F) con un sartén de hierro en el fondo.
1. ~5min antes de meterlos, agregar 1/2 litro de agua al sarten para aumentar la humedad del horno.
1. Con la ayuda de una tabla, pasar los birotes a una pala, con el doblez hacia abajo.
1. Hacer un corte sesgado a lo largo con una navaja de afeitar. Rápido y limpio.
1. Horner por 18-20 min, hasta que tengan un color dorado.
1. Dejar reposar en una canasta o rejilla por 30 min antes de comer.
