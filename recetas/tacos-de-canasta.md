Tacos de canasta
================

Basada en: [como hacer TACOS DE CANASTA, la receta secreta de los
taqueros, \# 456 \| Chef
Roger](https://www.youtube.com/watch?v=kH0nLhOtY2s) y [TACOS AL VAPOR
ESTILO JALISCO - Alejandra de
Nava](https://www.youtube.com/watch?v=PoDRiJ3zVmc)
También llamados tacos sudados. Aunque son similares a los [tacos al vapor](tacos-al-vapor.md), el
procedimiento de armado es diferente.

## Ingredientes

-   ~100 tortillas [^1]

**Aceite**

-   20 chiles guajillos

-   3/4 de litro de aceite

-   6 dientes de ajo

**Chicharron prensado**

-   500gr de chicharron prensado

-   7 chiles guajillos

-   1/4 litro de agua

-   1/4 litro de aceite

-   2 dientes de ajo

**Puré de papa**

-   500gr de papa

-   1/2 cebolla

-   1 diente de ajo

-   3 cucharadas de aceite

**Frijoles**

-   500 gr de rijoles refritos. Ver [receta](frijoles-refritos.md).


## Instrucciones

**Chicharrón prensado**

-   Remover las semillas a los chiles y dejar hervir por ~5 min,
    hasta que esten suaves.

-   Licuar con el aceite, el agua y el ajo.

-   Colar.

-   Partir el chicharrón en cuadritos.

-   Poner a calentar y desbaratar, hasta que queden sólo hebras.

-   Agregar la salsa de guajillo y llevar a hervor. Tratar de que queden
    medio secos, no es necesario agregar toda la salsa.

**Puré de papa**

-   Cocer la papa pelada hasta que este suave.

-   Machacar.

-   Picar la cebolla y ajo.

-   En un sartén dorar la cebolla y el ajo por unos segundos.

-   Mezclar todo.

**Frijoles refritos**
Ver [receta](frijoles-refritos.md). Se necesitan dejar medio secos.
**Tacos**

1.  Remover las semillas a los chiles y dejar hervir por ~5 min,
    hasta que esten suaves.

2.  Licuar con el aceite y el ajo.

3.  Colar el aceite.

4.  En una canasta o hielera poner las siguientes capas: toalla, papel
    estraza, bolsa de plástico grande y papel estraza.

5.  Pasar un lado de las tortillas (calientes) por el aceite
    (caliente)[^2].

6.  Poner el guiso (caliente) por el lado en donde se pasó el aceite y
    cerrar.

7.  Acomodar una capa de tacos en la canasta. Tapa la canasta con algo
    después de poner cada taco para que no se enfríen.

8.  Desoués de cada capa echa algunas cucharadas de aceite encima y
    distirbuye[^3].

9.  Al terminar todas la capas subre con papel estraza, luego cierra la
    bolsa y al final cubre con otro toalla.

10. Dejar deposar por 1hr.

[^1]: De preferencia recien hechas para que no se rompan. Si son de
    máquina probablemente necesite llevar tortilla doble.

[^2]: Si se usa tortilla doble los lados que van por enmedio también se
    deben de pasar por aceite

[^3]: Si quieres hacerlo light y no las cubres bien de aceite se pueden
    desbaratar. Este pedo no es light.
