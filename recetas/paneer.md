Paneer
======

Basada en : [Homemade paneer - Madhura's
recipe](http://www.madhurasrecipe.com/veg/Homemade-Paneer)

## Ingredientes

-   1 galón (3.8L) de leche pasteurizada [^1]

-   1/2 taza de jugo de limon


## Instrucciones

1.  Calentar la leche a punto de ebullición y apagar.

2.  Agregar el limón y esperar unos minutos a que cuaje.

3.  Poner en un molde con estameña y exprimir el suero excedente [^2].

4.  Dejar que escurra por un par de horas.

[^1]: De preferencia pasteurizada a bajas temperaturas y no
    homogeneizada. Quitar crema si no está homogeneizada. No usar leche
    ultra-pasteurizada.

[^2]: Quedó muy aguada, quizá debí de exprimir más?
