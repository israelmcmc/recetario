Arroz verde
===========


## Ingredientes

-   2 chiles poblanos (~250gr)

-   ~20 ramas de cilantro (o al gusto)

-   2 cucharaditas de aceite

-   1 taza de arroz corto

-   2 tazas de agua

-   Cubo de caldo de pollo (11gr)

-   Cuarto de cebolla (~70gr)

-   de cucharadita de sal (o al gusto)

-   de cucharadita de ajo en polvo (o al gusto)

-   Verdura cocida picada al gusto (zanahoria, elote o chicharo)


## Instrucciones

1.  Se ponen los chiles a fuego directo hasta que estan casi totalmente
    quemados homogeneamente. Se envuelven en plastico y se dejan reposar
    por 10min. Se pelan y remueven semillas y venas.

2.  *Opcional*: Se lava el arroz hasta que el agua salga relativamente
    clara (~3 veces).

3.  Se pone a hervir el agua y se disuelve el cubo de pollo. Se licua
    junto con los chiles, el cilantro, el ajo y la sal.

4.  Se precaliente un sartén con el aceite y se añade el arroz. Se
    mantiene en movimiento hasta que el arroz este amarillo. Se agrega
    la cebolla picada y se continua moviendo hasta que tenda un color
    dorado sin quemarse.

5.  Se agrega y revuelve en el sartén la mezcla licuada.

6.  Se deja a fuego lento, hasta que toda el agua se haya evaporado pero
    antes de que se pegue el arroz. Agregar las verduras un poco antes
    de esto si se desea.
