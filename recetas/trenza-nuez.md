Trenza de nuez
==============

Basada en: [Trenza de hojaldre rellena de queso crema y nuez - La cocina
mexicana de
Pily](http://www.lacocinamexicanadepily.com/2016/12/trenza-de-hojaldre-rellena-de-queso-crema-nuez-y-miel/).
Cambié la masa de hojaldre por masa de croissant para mejorar la
textura, inspirado por la trenza de almudevar.


## Ingredientes

-   550 gr de

-   1 paquete de queso crema (8oz, 227gr) a temperatura ambiente

-   1/3 de taza de azucar

-   200 gr nuez picada

-   ~4 cucharadas de miel

-   Mantequilla para la charola.


## Instrucciones

1.  Mezclar el queso con el azúcar

2.  En una superficie enharinada estirar la masa hasta formar un
    rectángulo de ~30x75 cm.

3.  Con la ayuda de una manga de repostería y una espátula poner una
    capa de queso con azúcar sobre la masa.

4.  Esparcir la nuez sobre el queso.

5.  Enrollar todo a lo largo.

6.  Cortar el rollo en dos a lo largo.

7.  Trenzar las dos mitades, enrollandolas una sobre en la otra, con la
    parte abierta hacia arriba.

8.  Pasar a una charola engrasada y hacer un círculo, tratando de
    acomodar lo mejor posible las orillas.

9.  Dejar reposar por ~1hr hasta que cada capa de masa doble su
    grosor

10. Hornear a 180 C (350 F) por ~40 min (hasta que esté dorada por
    completo).
