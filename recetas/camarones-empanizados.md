Camarones empanizados
=====================

Basada en: [FILETE DE PESCADO EMPANIZADO \| Vicky Receta
Facil](https://www.youtube.com/watch?v=1pj0MG30yls)
Misma receta para el pescado empanizado.


## Ingredientes

-   1/4 de taza de leche

-   1 huevo

-   Harina

-   Pan molido

-   24 camarones

-   Sal

-   Pimienta

-   Aceite chingos


## Instrucciones

1.  Batir el huevo y la leche.

2.  Pelar los camarones y secarlos.

3.  Hacer una rajada en la mitad gorda y abrir en mariposa.

4.  Espolvorear con sal y pimienta.

5.  Pasar por la harina.

6.  Luego por la leche y huevo.

7.  Luego por el pan molido. De preferencia dejar los camarones
    cubiertos por el pan molido por un rato.

8.  Freir en aceite a fuego medio hasta que esten dorados (~2 min)
