Pan de calabaza
===============

## Ingredientes

Para molde redondo, duplicar para molde de panqué

-   1 taza de harina

-   1/2 + 1/3 tazas de azucar

-   1/2 cucharadita de bicarbonato

-   1 cucharadita de levadura química

-   1/2 de cucharadita de sal

-   1/2 de taza de aceite vegetal

-   2 huevos

-   7.5 oz de pulpa de calabaza

-   1 cucharadita de canela molida

-   Harina y mantequilla para el molde

[Instrucciones pulpa de calabaza]{.underline}

1.  Limpiar la calabaza y cortarla en gajos

2.  Poner en el horno a 200° (400°F) por una hora o hasta
    que este blanda.

3.  Quitar cáscara y moler en procesador.


## Instrucciones

1.  Hacer una mezcla homogenea.

2.  Cocinar en el horno a 180° (350°F) por 20 min en molde
    redondo o ~80 min en molde de panqué (hasta que esté sólido,
    sin secarse)
