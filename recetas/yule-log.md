Yule log
========

Fuente: [Joy of Baking - Yule
log](http://www.joyofbaking.com/YuleLog.html)

## Ingredientes

**Ganache**

-   6 oz de chocolate oscuro

-   2 oz de chocolate dulce (mil chocolate)

-   3/4 de taza de heavy whipping cream

-   25 gr de mantequilla

**Esponja**

-   1/4 de taza de harina

-   2 cucharadas de maicena

-   2 cucharadas de cocoa

-   1/8 de cucharadita de sal

-   1/2 taza de azúcar

-   2 huevos enteros

-   3 yemas de huevo

-   2 claras de huevo

-   1 cucharadita de vainilla

-   1/4 de cucharadita de crema tartara

-   Un poco de azucar glass

**Relleno**

-   1 taza de heavy whipping cream (fría)

-   1/2 cucharadita de vainilla

-   1/4 de taza de azucar

-   2 cucharadas de cocoa


## Instrucciones

**Ganache**

1.  Calentar la whipping cream y la mantequilla hasta que comienze a
    hervir y sacar de la estufa.

2.  Añadir el chocolate y mezclar hasta que se derrita y este uniforme.

3.  Dejar reposar hasta que se enfríe y este untable. Si se mete el
    refri para acelerar el procesos menear cada 10 min.

**Esponja**

1.  Cubrir de mantequilla un bandeja, poner un papel encerado, luego
    otra capa de mantequilla y enharinar.

2.  Batir a alta velocidad los huevos enteros, las yemas, la vainilla y
    la azúcar menos una cucharada (guardar) por ~3 min hasta que
    este espesa.

3.  Batir la crema tartara y las claras por unos segundos. Añadir la
    cucharada de azucar y batir a punto de turron.

4.  Mezclar los dos batidos y el resto de los ingredientes. Hacerlo de
    forma suave.

5.  Esparcir de forma uniforme en la bandeja.

6.  Hornear a 450 F (230 C) por 6 min. Que este bien cocido pero evitar
    que se seque y endurezca.

7.  Espolvorear la azucar glass sobre la esponja.

8.  Volvear sobre una toalla

9.  Espolvorear azucar glass sobre el otro lado

10. Enrollar y dejar que se enfrie

**Relleno**

1.  Batir todo hasta que este bien firme

**Final**

-   Desenrollar la esponja

-   Untar el relleno de forma uniforme por dentro

-   Enrollar

-   Cubrir con el ganache
