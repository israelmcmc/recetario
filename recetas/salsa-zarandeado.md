Salsa para zarandeado {#salsa-zarandeado}
=====================

Basada en : [Pescado zarandeado estilo Sinaloa - APRENDE A COCINAR Con
el Chef
Vizcaino](https://aprendeacocinarfacil.wordpress.com/2012/03/13/pescado-sarandeado-estilo-sinaloa-aya-pinchi/)
Ver [zarandeado](zarandeado.md), aunque también le queda bien pescado en general.

## Ingredientes

-   5 jitomates en cuadritos

-   1 chile anaheim o california fresco en aros

-   1 bara de apio

-   3 chile serrano

-   1/2 de cebolla

-   1/4 de tazade de chicharos

-   1/4 de taza de aceitunas

-   1/4 de cucharadita de oregano

-   1 pizca de comino

-   3 dientes de ajo machacados

-   30 gr de mantequilla

-   2 cucharadas de aceite de oliva

-   Sal al gusto (~2 cucharaditas)

-   Pimienta al gusto ( ~1/2 de cucharadita)


## Instrucciones

1.  Guisar la cebolla y el apio en el aceite.

2.  Agregar la mantequilla hasta que se derrita.

3.  Agregar el resto de los ingredientes y agua la necesaria (poca).

4.  Cocinar por ~25 min.
