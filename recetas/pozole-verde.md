Pozole verde
============


## Ingredientes

-   750 g de nixtamal (maiz)

-   2 1/2 ajos

-   1 1/2 cebollas

-   2 hojas de laurel

-   ~9 litros de agua

-   sal de grano

-   2 chiles poblanos

-   2 chiles jalapeños

-   1 chile serrano

-   250 g de tomatillo

-   1 tz de pepita de calabaza

-   Hierbas verdes (1 ramita de epazote, 6 hojas de espinaca, 1 ramo de
    cilantro, 1 ramo de perejil y lechuga)

-   5 pimientas negras

-   3 clavos de olor

-   Manteca de cerdo

-   1 cdita de comino

-   3 cdas de oregano

-   1.5 kg de carne por cada 750 g de maiz (maciza,
    cabeza, espinazo, etc.. al gusto de preferencia con hueso para el
    caldo).


## Instrucciones

1.  Empezando con maiz nixtamalizado, lavar bien y verificar que este
    bien descabezado el grano.

2.  En agua caliente, se pone a cocer el maiz, dos ajos enteros (mochos
    para que suelten sabor pero sin pelar), las hojas de laurel y una
    cebolla grande (mocha o con una cruz para que suelte el sabor).

3.  Se parte la carne y se agrega al maiz, junto con un poco de sal.

4.  Cocer tomatillos y chiles jalapeños en agua. Por separado, dorar 3
    dientes de ajo y media cebolla en manteca. Al final, se reserva la
    manteca y se licuan la cebolla y el ajo junto con los tomatillos,
    chiles, pimientas, clavo y dos tazas de agua.

5.  En otra tanda se licuan el resto de las hierbas (epazote, espinaca,
    cilantro, etc..) junto con las pepitas tostadas 1 cda de
    oregano y sal.

6.  Ahora se recuecen las dos salsas en la manteca y la mezcla se deja
    hervir a fuego lento con un puñito de oregano triturado.

7.  Se sacan los ajos, la cebolla y las hojas de laurel del caldo de
    puerco y se agrega la salsa verde. Se deja a fuego lento y se agrega
    sal al gusto.

8.  Se sirve con chicharron, cebollita picada finamente, rabano,
    lechuga, tostadas, aguacate y limon.
