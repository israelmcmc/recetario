Salsa macha
===========


## Ingredientes

-   50 gr de cachuate

-   ~4 dientes de ajo

-   15 gr de chiles de árbol

-   25 gr de chile morita

-   1 cucharadita de sal

-   2 cucharadas de ajonjoli

-   ~1/4 de cebolla

-   ~1 taza de aceite vegetal


## Instrucciones

1.  Freir las cebollas en el aceite hasta que esten transparentes.

2.  Freir los ajos hasta que esten dorados.

3.  Dorar los chiles, sin quemarlos.

4.  Freir los cachuates hasta que esten dorados

5.  Apagar el fuego y echar el ajonjolí.

6.  Licuar todo, incluyendo el aceite.
