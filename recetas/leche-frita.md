# Leche frita

Basado en [Yum China - Fried milk](https://www.yumofchina.com/fried-milk) y [Ana recetas fáciles - Leche frita](https://www.youtube.com/watch?v=vCnvUe3kx0k)

## Ingredientes
- 500 ml de leche
- 50 gr de azúcar
- 60 gr de maicena
- Cáscara de una naranja
- 1 raja de canela
- 1/2 cucharadita de vainilla
- Harina
- 1 huevo
- Pan para empanizar
- Aceite
- Azúcar glass

## Instrucciones
1. Hervir la mitad de la leche con la canela, el azúcar y la canela. Por 10 min a fuego muy bajo.
1. En la otra mitad de la leche disolver la maicena y la vainilla.
1. Colar la mitad caliente y mezclar ambas partes.
1. Hervir a fuego bajo batiendo constantemente hasta que se espese
1. Poner en un molde tal que toda la mezcla tengaa 3-4cm de espesor.
1. Dejar enfriar a temperatura ambiente y luego en refrigerador por unas horas, hasta que se solidique.
1. Empanizar. Pasar por la harina, huevo y pan, en ese orden. Freir hasta que esté dorado.
1. Espolvorear con azúcar glass.
