Pan de anis de la abuela de Paco
==============

Basada en: receta original de Lilia Ledezma Zavala.

## Ingredientes

-   1 1/2 tazas de harina

-   1 cdita de polvo para hornear

-   1/2 taza de leche tibia

-   4 cdas. de mantequilla derretida

-   1/2 taza de infusion de anis (se hierve agua con 1 cda. de semillas de anis)

-   1 taza de piloncillo rayado

-   Mantequilla para engrasar la charola.


## Instrucciones

1.  Se mezcla el harina con el polvo para hornear

2.  Se incorpora la leche, la mantequilla, y el anis y se forma una pasta homogenea. Se pueden dejar las semillas de anis para mas aroma.

3.  En un molde engrasado se vierte la mezcla.

4.  Esparcir el piloncillo sobre la superficie.

10. Hornear a 180 C (350 F) por ~25 min (hasta que el pan se haya cocido y el piloncillo se haya caramelizado).
