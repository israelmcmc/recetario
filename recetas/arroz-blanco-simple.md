Arroz blanco simple
===================


## Ingredientes

-   1 taza de arroz

-   1 1/2 tazas de agua


## Instrucciones

1.  En un sartén grande agregar el arroz y el agua.

2.  Cocinar a fuego alto hasta que hierva.

3.  Bajar al fuego MUY bajo y tapar.

4.  Apagar cuando no quede agua.
