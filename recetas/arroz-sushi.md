Arroz para sushi
================


## Ingredientes

-   1 1/2 tazas de arroz medio/largo

-   2 tazas de agua

-   1/4 de taza de vinagre

-   1 cucharada de azúcar

-   1 cucharadita de sal


## Instrucciones

1.  Agregar el agua y cocinar a fuego alto hasta que comience a hervir.

1.  Cocinar hasta que se evapore toda el agua, a fuego bajo tal que dura 20 min.

1.  Mezclar el vinagre, azucal y sal. Revolver la mezcla con el arroz.

1.  Utilizar hasta que se enfríe.
