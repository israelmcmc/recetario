Tamales cuadrados tabasqueños
=============================

Receta de Lety, la tía de Kris.

## Ingredientes

**Relleno**

-   1 pavo [^1]

-   5 ramas de epazote fresco

-   5 chiles anchos

-   5 chiles mulato

-   4 chiles guajillo

-   1 taza de ajonjolí

-   1 taza de semilla de calabaza con cascarita

-   1 cebolla morada chica

-   1 cabeza de ajo chica

-   2 tomates

-   186 gr de galleta soda (1 paquete)

-   4 clavos de olor

-   1 pizca de orégano negro

-   1 tortilla gruesa

-   2 oz de mole Doña María

-   ~1 taza de aceite

-   Sal

-   Agua

**Masa**

-   3 kg de maiz

-   1 l de aceite o manteca de pavo

-   ~10 l de agua

-   Sal

**Armado**

-   ~30 mitades de hoja de platano [^2] (sin el tallo de en medio)


## Instrucciones

**Relleno**

1.  Aliña el pavo cortándolo en ~30 pedazos. Las alas se separan
    en dos y las piernas se parten en dos a lo ancho.

2.  Limpiar los chiles y cocer por unos minutos hasta que se aguaden.

3.  En el aceite dorar los siguiente, en este orden y sin que se quemen:

    1.  Las galletas soda

    2.  La tortilla gruesa

    3.  La cebolla y los ajos (hasta que se acitrone la cebolla).

    4.  El epazote.

    5.  El ajonjolí y la semilla de calabaza. En este punto ya casi no
        debería de quedar aceite, agregar más de ser necesario.

4.  Picar el tomate y guisar en lo que quedó de aceite, el oregano y el
    clavo.

5.  Moler el ajonjolí, los clavos y la semilla de calabaza.

6.  Licuar todo con suficiente agua para que quede un mole aguadito.

7.  Colar

8.  Corregir de sal.

9.  Dejar las piezas de pavo (crudo) en el mole una noche.

**Masa**

1.  Cocer el maiz[^3] en suficiente agua y llevarlo a ebullición.

2.  Apagar y colar el agua.

3.  Moler. Dos veces si no queda fino a la primera. Debe de quedar lo
    suficientemente fino para hacer bolitas sin que se desbaraten.

4.  Disolver con ~10 l de agua. Se debe de ver como una horchata
    bien cargada.

5.  Colar con un colador grueso y luego por uno fino. Descartar el
    residuo.

6.  Agregar la sal, suficiente para que sepa saladita.

7.  Agregar el aceite.

8.  Poner a cocer a fuego medio-alto, meneando constantemenete y
    raspando el fondo. ~15 - 20 min después notarás que empieza a
    ofrecer resistencia. Seguir meneando fuertemente por ~10 min
    hasta que de un hervor.

9.  Dejar a que se enfríe por ~10 min.

**Armado**

1.  Soasar las hojas para suavisarlas. Con el lado brilloso hacia abajo,
    se pasan unos segundo por el fuego hasta que el lado opaco cambie de
    color.

2.  Cortar en trozos de ~40 cm x 30 cm. Salen aproximadamente 3
    por cada mitad de hoja, pero algunas se rompen.

3.  Envoltura:

    1.  Poner dos hojas con el lado rugoso hacia abajo y el tallo hacia
        afuera.

    2.  Poner 1 taza de masa

    3.  Poner la pieza del pollo

    4.  Agregar una cucharón pozolero de guiso.

    5.  Envolver: lados izquierdo y derecho interior, izquierdo y
        derecho exterior, abajo, se para el tamal, y arriba.

4.  Cocer en vaporera por 3 hr [^4]. Deja enfriar antes de servir para
    que cuaje la masa.

[^1]: O 4 pollos

[^2]: En principio se necesitan 20, pero muchas suelen estar rotas. Si
    se está escaso de hojas se pueden usar la mitad y cada tamal lleva
    una sóla hoja sin la de refuerzo reglamentaria.

[^3]: Me parece que sería mejor dejarlo remojando una noche primero para
    facilitar la molienda, pero no lo he intentado

[^4]: Este tiempo es para que se cueza la carne. Para pollo son ~2
    hr. Para gallina vieja hasta 5 hr. Sin carne basta con 1 hr
