Torta de calabacita
===================

Basada en: recetario abuelita

## Ingredientes

-   3/4 kg de calabacita

-   1/4 kg de harina de arroz

-   2 cucharaditas de polvo para hornear

-   100 gr de mantequilla (temperatura ambiente)

-   3 huevos

-   100 gr de queso cotija

-   1 taza de azúcar

-   Mantequilla y pan molido para el molde.


## Instrucciones

1.  Poner a hervir agua con un poco de sal y cocer las calabacitas por 5
    min. Las calabacitas van enteras o sólo partidas a la mitad.

2.  Dejar escurrir las calabacitas y luego licuarlas.

3.  Acremar la mantequilla con el azúcar.

4.  Batir con los huevos.

5.  Añadir la calabacita molida, la harina arroz, el polvo para hornear
    y el queso, y mezclar.

6.  Recubrir el molde con mantequilla y pan molido.

7.  Hornear a 180C (350F) por ~40min, hasta que pase la prueba dle
    palillo. En moldes para muffin sólo necesita ~25min.
