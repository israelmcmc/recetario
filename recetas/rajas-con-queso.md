Rajas con queso
===============


## Ingredientes

-   8 chiles poblanos

-   3/4 de taza de elote

-   2 rebanadas de cebolla

-   4 cucharadas de crema

-   1 cubito de caldo de pollo.

-   2 cucharada de aceite

-   70 gr de queso rayado


## Instrucciones

1.  Preparar los chiles

    1.  Quemar

    2.  Sudar

    3.  Pelar

    4.  Quitar semillas

    5.  Cortar

2.  Sofreir la cebolla, rajas y elote en aceite.

3.  Agregar el cubo de pollo y la crema, y dejar que hierva un par de
    minutos.

4.  Agregar queso rayado
