Pay de queso
============


## Ingredientes

-   Un paquete de galletas maría (170gr)

-   Una barra de matequilla (113gr) + engrasar molde

-   Un paquete de queso crema (8oz)

-   2 huevos

-   1 cucharadita de vainilla

-   lata de leche condensada (200gr)

-   lata de leche evaporada (170gr)

[Preparaión]{.underline}

-   Dejar que la mantequilla este a temperatura ambiente

-   Hacer polvo las galletas maría.

-   Hacer una masa homogenea con el polvo de galletas y la mantequilla

-   Engrasar el molde con una capa de mantequilla.

-   Poner una capa homogenea de la masa de galletas y mantequilla en
    todo el molde.

-   Licuar el resto de los ingredientes y verter en el molde

-   Hornear por 45 min a 180C (350F).

-   Sacar y dejar que llegue a temperatura ambiente. Despues meter el
    refrigerador y dejar que llegue a la temperatura interna de este.
