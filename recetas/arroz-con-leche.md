Arroz con leche
===============

Basada en: [CÓMO HACER ARROZ CON LECHE \| Vicky Receta
Facil](https://www.youtube.com/watch?v=tJkeaqp3ULg)

## Ingredientes

-   2 tazas de arroz

-   1 lata de leche condensada (397gr)

-   1 lata de leche evaporada (360 gr)

-   1/2 litro de leche

-   2 raja de canela

-   1 1/1 litro de agua

-   1 cucharadita de vainilla

-   Canela en polvo

-   Pasitas


## Instrucciones

1.  Lavar el arroz

2.  Llevar el agua a ebullición con las rajas de canela

3.  Echar el arroz y cocer a fuego bajo hasta que este blando (~
    20min)

4.  Retirar las rajas de canela

5.  Agregar las tres leches y la vainilla.

6.  Dejar cocer por ~10 min sin dejar de menear hasta que espese.

7.  Dejar reposar para que se espese más.

8.  Servir con canela en polvo y pasitas.
