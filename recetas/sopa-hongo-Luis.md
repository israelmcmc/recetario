Sopa de hongos de Luis
======================


## Ingredientes

-   kilo de hongos (e.g. baby bella, shitake, oyster)

-   1 jitomates huaje o equivalente

-   de cebolla

-   1 cucharada de aceite

-   3 dientes de ajo

-   ~1 cucharadita de sal

-   Epazote, perejil, cilantro u oregano al gusto

-   ~2 tazas de agua


## Instrucciones

1.  Licuar el jitomate, la cebolla y el ajo

2.  Sofreir en el aceite hasta que se pierda la mayor parte del agua

3.  Agregar la sal y las especias.

4.  Agregar los hongos y cocer a fuego medio hasta que comiencen a
    soltar agua.

5.  Agregar el agua hasta que casi tape los hongos

6.  Hervir a fue alto hasta que el caldo tenga la consistencia adecuada.
