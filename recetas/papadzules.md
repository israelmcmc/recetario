# Papadzules

Basada en [CÓMO PREPARAR PAPADZULES YUCATECOS | La Tía Zazil](https://www.youtube.com/watch?v=EtsB3vufFC4&ab_channel=LaT%C3%ADaZazil)

## Ingredientes

- 5 tortillas
- 6 huevos cocidos

### Salsa verde
- 250 gr. de semillas de calabaza
- 1 manojo de epazote
- 1 L de agua
- ~1 cdita sal

### Salsa roja
- 4 jitomate.
1/2 cebolla blanca
2 dientes de ajo
2 cucharadas de aceite
4 chiles habaneros
- ~1 cdita de sal

### Instrucciones

### Salsa verde
1. Hervir el empazote por ~5 min en el agua. Remover.
1. Tostar y moles las pepitas. De preferenia en molino o molcajete.
1. Agregar al agua y cocer hasta que espese (10-15 min). Meneando.

### Salsa roja
1. Cocer los jitomates, chiles, cebolla y ajo
1. Licuar
1. Guisar en el aceite y reducir.
