# Sopa de cebolla

Basada en The french chef cookbook de Julia Child

## Ingredientes
- 3 cucharadas de mantequilla
- 1 cucharada de aceite de oliva
- 1.5 kg de cebolla [^1] [^2]
- ~1.5 cucharadita de sal
- 1/2 cucharadita de azúcar
- 2 cucharadas de harina
- 1 1/2 litros de caldo de res
- 1 taza de vino (tinto o blanco)
- 1 hoja de laurel molida
- 1/2 cucharadita de salvia picada
- ~1/8 de cucharadita de pimienta

### Gratinado
- 1 baguette
- Mantequilla o aceite
- ~100 gr de queso gruyere
- ~200 gr de queso provolone
- ~100 gr de queso parmesano
- Unas rebanadas finas de cebolla

## Instrucciones
1. Partir la cebolla en finas rebanadas y acitronar. ~20 min a fuego bajo, meneando cada ~10 min, hasta que le haya salido agua.
1. Agregar la sal y el azucar, y dejar dorar. ~30 min a fuego medio bajo. Menear aproximadamente a los 10 min, 20 min, 25 min y 28 min. Cuando ya casi no tiene agua y se está dorando se debe de menear constantemente.
1. Agregar la harina y cocer por ~2 min. Se forma una pasta.
1. Agregar un poco del caldo y disolver la pasta. 
1. Agregar el resto del caldo, el vino, la salvia, la hoja de laurel y pimienta. Cocer por ~30 min a fuego bajo.

### Gratinado
1. Pasar rebanadas de pan por el aceite o mantequilla y tostar.
1. En un ramekin (~1/2 litro, salen ~5) poner la sopa calienta, luego las rebanadas de cebolla crudas, lugo el pan, y luego el queso rallado.
1. Hornear a 230 C (450 F) por ~15 min, hasta que este dorado el queso.

[^1]: Ó 1000 gr de cebolla y 500 gr de shallots.
[^2]: La receta original tiene sólamente 750 gr de cebolla en total, pero se me hizo muy sin chiste.
