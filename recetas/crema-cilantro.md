# Crema de cilantro

Basada en [CREMA DE CILANTRO | #VickyRecetaFacil](https://www.youtube.com/watch?v=cGrKQF-rWSo)

## Ingredientes

- 1 manojo cilantro (~50 gr)
- 45gr mantequilla
- 1 rebanada de cebolla finamente picada (~70 gr)
- 150 ml de crema
- 1/2 litro agua
- 2 pizcas pimienta (blanca, gorda o negra)
- 2 cubitos de caldo de pollo
- 1/4 de taza de harina
- ~1/2 cucharadita de sal

## Instrucciones

1. Derretir la mantequilla a fuego medio
1. Acitronar la cebolla
1. Agregar la harina y batir, que no queden grumos
1. Licuar el cilantro con la crema, el caldo de pollo y el agua. Agregar a la olla
1. Agregar la pimienta y sal.
1. Llevar a hervor.