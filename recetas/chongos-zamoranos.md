Chongos zamoranos
=================


## Ingredientes

-   1 galón de leche (3.8L) [^1]

-   500 gr de piloncillo

-   1/4 pastilla de cuajar [^2]

-   2 o 3 rajas canela


## Instrucciones

1.  Disolver la pastilla en un poco de agua.

1.  Calentar la leche hasta que esté tibia --~32C--.

1.  Agregar la pastilla disuelta a la leche y esperar a que cuaje
    (~1 hr). Esta lista cuando se puedar cortar con un cuchillo y sale limpio.

1.  Cortar en 8 pedazos diagonalmente (en forma de asterisco). 

1.  Reposar por 30 min

1. Poner a fuego bajo por ~30 min. Es importante que no hierva fuerte 
   para que no se desbaraten los trozos de cuajo.

1. Sacar la suficiente cantidad de suero para disolver el piloncillo.

1. Disolver el piloncillo y devolver todo a la olla principal.

1. Clavar trozos de canela quebrada entre los trozos de cuajo.

1. Dejar a fuego muy bajo por 3 horas, sin mover.

1.  Voltear los trozo de cuajo con cuidado y dejar a fuego medio por ~2 horas,
    hasta que el suero se haya reducido y sólo quede un poco más de mitad en la olla.

1. Poner en frascos, dejar enfriar a temperatura ambiente, y luego en refrigerador. 

[^1]: Usar de preferencia leche bronca. Si se usa pasterurizada, de
    prefencia que haya sido pasteurizada a temperaturas bajas y que no
    esté homogeneizada. No usar leche ultra-pasteurizada.

[^2]: Usar pastillas para cuajar que se utilicen en la fabricación de
    queso. No usar \"Junket\". La cantidad puede variar dependiendo de
    la marca. Usé pastillas Walcoren que cuajan 20 litros por pastilla.
