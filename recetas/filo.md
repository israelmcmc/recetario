# Pasta filo

Basada en [Food wishes - Homemade FIlo or Phyllo Dough](https://www.youtube.com/watch?v=Ams-Sb4l2iE)

## Ingredientes

### Masa
- 300 gr de harina (2 tazas)
- 5 cucharaditas de aceite de oliva (30 gr)
- 1/2 cucharadita de sal (3 gr)
- 2 cucharaditas de vinagre de vino (10 gr)
- 3/4 de taza de agua tibia (190 gr)

### Mezcla de maicena para espolvorear
- 1/2 taza de maicena
- 2 cucharadas de harina

## Instrucciones
1. Mezclar la masa y amasar hasta que esté lisa (~5 min a mano)
1. Dejar reposar por ~1 hr
1. Agarrar una bolita de 20 gr, espolvorear, y extender hasta un diámetro de ~9 cm (tamaño de palma)
1. Repetir con 9 bolitas. Las láminas se van apilando espolvoreando abundantemente entre ellas.
1. Extender las láminas apiladas hasta que tengan un diámetro de ~15 cm (una mano)
1. Separar una por una y volver a apilar, espolvoreando de nuevo.
1. Volver a extender hasta que tenga un díámetro de ~25 cm [^1].
1. Separar. Espolvorer más mezcla de maicena entre ellas si se va a guardar. 
1. Repetir con el resto de la masa (~27 láminas en total).


[^1]: Esto da ~25 cm^2/gr de masa, lo cual es considero grueso. La past [pasta comercial](https://www.fillofactory.com/) va de 30 cm^2/gr a 80 cm^2/gr.
