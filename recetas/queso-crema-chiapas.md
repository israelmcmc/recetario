Queso crema de Chiapas
======================

Basada en : [Queso Crema de Chiapas - 
Arturo Hernández Montes - UACh](https://www.youtube.com/watch?v=hJAG7uoCcwg)
y Los quesos mexicanos tradicionales, de Villegas, et al.

## Ingredientes

-   1 galon de leche

-   1/4 de pastilla para cuajar [^1]

-   20 - 30 gr de sal

-   Opcional: 1/8 de cucharadita de inoculador mesofílico [^2]

-   Opcional: 1 cucharadita de cloruro de calcio al 32% en peso [^3]

## Instrucciones

1. Agregar el inoculador (opcional para leche bronca).

1. Dejar reposar a temperatura ambiente (<25C) por ~12 hr.

1. Agregar el cuajo y dejar reposar por ~12 hr.

1. Hacer cortes con cuchillo, gruesos (~10cm).

1. Dejar deposar por ~12 hrs.

1. Dejar desuerar en un colador. Sin apretar, sólo por gravedad. Aproximadamente ~12 horas.

1. Agregar 4% en peso de sal.

1. Amasar hasta que el grano sea muy fino.

1. Prensar (~0.3 kg/cm^3) por ~12 hrs hasta que este bien firme.



[^1]: Esto es para la marca Walcoren. 1 pastilla completa para 16L

[^2]: Esto es para el Danisco Choozit MA 11. Seguir indicaciónes para
    otra marca. Se necesita el mismo tipo que para hacer fetta. Si
    la leche no esta pasteurizada, se puede usar el método tradicional y
    no se necesita, aunque es más rapido y reproducible.

[^3]: Para leche pasteurizada, no se necesita para leche bronca.

[^6]: En lugares fríos quizá sea necesario mantener la temperatura a
    ~35 C mediante baño María durante todo el proceso

[^7]: 32 - 35 D

[^8]: Una alternativa es ponerlos en una mesa y rociarlos con agua fría.
    Así quedan las tiras quedan planas como un liston.
