Chiles rellenos
===============


## Ingredientes

-   1 chile poblano

-   2 huevos

-   ~100 gr de queso para derretir (e.g. Oaxaca, Chihuahua,
    asadero)

-   Harina (para espolvorear)

-   Dos cucharadas de aceite


## Instrucciones

1.  Poner el chile al fuego hasta que este blandito y la mayor parte de
    la piel este quemada.

2.  Envolverlo con plástico y dejar que se enfríe.

3.  Remover la piel.

4.  Hacer una abertura por un lado y rellenar de queso.

5.  Espolvorear y cubrir con harina.

6.  Batir las dos claras de huevo hasta punto de turron. Cuando este
    listo, agregar media yema y batir lentamente. Descartar el resto.

7.  Cubrir con el turron y freír. Se empieza por donde esta la abertura
    pasa sellarlo. Con una espatula poner más turron si se descubrió
    alguna parte.

**Ingredientes salsa**

-   4 jitomates cocidos

-   2 tazas de agua

-   1 cuadrito de caldo de pollo (11 gr)

-   cuarto de cebolla.

-   1 cucharadita de ajo molido

-   1 cucharadita de sal

-   1 cuacharadita de oregano molido
