Flan napolitano
===============

## Ingredientes

-   1/2 taza de azúcar

-   4 huevos

-   1 lata de leche condensada (397 gr)

-   1 taza de leche evaporada

-   1 cucharadita de vainilla

-   10 oz de queso crema

## Instrucciones

1.  Calentar el azucar a fuego lento y meneando. Apagar tan pronto como
    toda este derretida.

2.  Verter la azucar derretida en el fondo de una flanera y dejar que
    llegue a temperatura ambiente.

3.  Licuar el resto de los ingredientes y verter en la flanera.

4.  Hornear en baño maría a 200°C (400F) por 1hr.

5.  Sacar del horno y esperar a que llegue a temperatura ambiente. Luego
    refrigerar por varias horas hasta que tenga la temperatura de
    equilibrio.

6.  Servir volteado.
