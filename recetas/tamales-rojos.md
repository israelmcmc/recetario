Tamales rojos
=============

Basada en [Vicky Receta Fácil](https://www.youtube.com/watch?v=y-_ozklQH7w).

[Ingredientes (~16 tamales)]{.underline}

-   ~30hojas de maíz secas

**Carne**

-   ~1 kg de carne de puerco

-   1 diente de ajo

-   5 pimientas negras

-   4 hojas de laurel

-   ~1/4 de cebolla

-   ~1 chucharada de sal

**Salsa**

-   ~1 cucharadita de sal

-   12 chiles guiajillos o anchos o combinación.

-   ~10 chiles de árbol

-   ~2 dientes de ajo

-   2 clavos

-   8 pimientas negras

-   ~1/4 de cebolla pequeña

-   2 jitomates

-   1/4 de cucharaditas de comino

-   1/2 cucharadita de orégano

**Masa**

1.  4 tazas de Maseca pra tamal

2.  3 tazas de consomé de pollo

3.  2 cucharadita de sal

4.  2 cucharaditas de polvo para hornear

5.  1 1/3 tazas de manteca de puerco o vegetal


## Instrucciones

**Carne**

1.  Cocer en en olla a presión con el resto de los ingredientes (20 min
    después de que comience a bambolear).

2.  Desmenuzar

**Salsa**

1.  Cocer los tomates hasta que revientes

2.  Remover semillas de chiles guajillos.

3.  Cocer los chiles por ~5min.

4.  Licuar todo

**Masa**

1.  Revolver la manteca hasta que este suave

2.  Agregar el resto de los ingredientes hasta que la masa este
    esponjosa

3.  Si hace falta, agregar un poco de agua hasta que la masa se pueda
    untar facilmente

**Tamales**

1.  Poner a remojar las hojas en agua caliente hasta que esten
    aguaditas.

2.  Preparar carne, salsa y masa.

3.  En una hoja, untar una capa de masa, luego un caminito de carne y un
    caminito de salsa.

4.  Doblar la hoja en tres y luego doblar el fondo.

5.  Poner agua y hojas en el fondo de un vaporea.

6.  Acomodar los tamales boca arriba (de preferencia completamente
    lleno, apretados).

7.  Clavar hojas de tamal a los lados y tapar.

8.  Cocer a fuego bajo for 1:45 horas.

[Notas]{.underline}

