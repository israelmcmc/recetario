Jericalla
=========


## Ingredientes

-   2 tazas de leche

-   2 huevos

-   1/2 taza de azucar

-   2 cucharadas de vainilla

-   1 raja de canela

[Preparación]{.underline}

1.  Poner a hervir todo excepto el huevo por 10 min, agitando
    continuamente.

2.  Esperar a que se enfríe.

3.  Revolver con el huevo.

4.  Poner a hornear a baño maría en moldes pequeños a 205°C (400°F)
    hasta que tenga manchas negras en la cubierta (~1.5 horas).
