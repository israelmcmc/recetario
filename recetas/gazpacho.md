Gazpacho
========


## Ingredientes

-   2 bolillos (o pan equivalente con migajon)

-   6 jitomates medianos

-   3 dientes de ajos grandes

-   1 pimiento morron verde

-   1 pimiento morron rojo

-   1 pepino

-   2 cucharadas de aceite extra virgen de oliva

-   1 cucharada de sal

-   1/4 de taza de agua


## Instrucciones

1.  Se remoja el bolillo y se deja aparte para agregar textura al gusto.

2.  Se licuan los jitomates junto con los pimientos, el ajo, la sal, el
    aceite y el agua.

3.  Se agrega el pan hasta lograr la textura mas deseable.

4.  Se pican cubitos de 1 cm de pepino y se sirven junto con un chorrito
    de aceite al gusto tambien.

5.  Se toma frio (yo siempre tengo todos los ingredientes en el
    refrigerador)

6.  Recomiendo media porcion como aperitivo a una paella.
