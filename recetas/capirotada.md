Capirotada
==========

Capirotada de Guadalajara. Basado en el recetario de mi mamá (y sus recuerdos y mi gusto, que no venían las cantidades apuntadas). 

## Ingredientes

-   1 jitomate pequeño picado (~150gr)

-   1/4 de cebolla pequeña picada (~70gr)

-   1 litro de agua

-   300 gr de piloncillo (panocha, panela)

-   4 clavos de olor

-   1 rajas de canela

-   250 gr de birote [^1]

-   Aceite [^2]

-   1 platano macho

-   1 taza de cacahuates

-   1/2 taza de pasitas

- 1 tortilla


## Instrucciones

1. Partir el birote en rebanadas gordas y dejar al aire libre hasta que se seque.

1. Dorar el birote en el aceite.

1.  Poner a hervir el agua con los jitomates, piloncillo, cebolla,
    clavos, canela y rebanadas de plátano por 15min.

1.  Colar todos los sólidos y rescatar el plátano. Reservar.

1. Pasar la tortilla por aceite y poner en el fondo de un cazuela [^3].

1. Agregar una capa de birote, luego platano, cachuates y pasitas. Seguir agregando capas hasta terminar.

1. Agregar todo caldo colado.

1. Hervir a fuego medio-bajo por ~15 min. El caldo se debe de absorber, pero sin que se seque mucho.

1. Servir fría.

[^1]: Un pan más blando que el birote se va a desbaratar.

[^2]: La receta original es con manteca

[^3]: La tortilla es porque normalmente se quema el fondo. Con una olla tipo Le Creuset no sea necesario.
