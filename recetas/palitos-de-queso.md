Palitos de queso croatas (receta de Juraj)
==========================================


## Ingredientes

-   250 gr de mantequilla

-   250 gr de queso cottage

-   250 gr de harina

-   1 cdita de sal


## Instrucciones

La única instrucción que nos dejó Juraj es hornear a 400 F, el resto
de las instrucciones están inspiradas en nuestra memoria\...

1.  Precalentar el horno a 400 F.

2.  Hacer una masa con todos los ingretientes.

3.  Extender la masa en una superficie enharinada hasta que quede tenga
    approx medio centímetro de grosor.

4.  Cortar en rectangulos largos approx 2 cm por 10 cm

5.  Hornear hasta que estén dorados.
