Galletas de avena
=================


## Ingredientes

-   1 taza de mantequilla suavizada

-   1 taza de azucar morena (mascabado)

-   2 huevos

-   1 cucharadita de vainilla

-   1 1/2 tazas de harina

-   1/2 cucharadita de sal

-   1 cucharadita de bicarbonato

-   1 cucharadita de canela en polvo

-   3 tazas de avena

-   1/2 taza de pasas o arandanos al gusto


## Instrucciones

1.  Se baten por separado la mantequilla, el azucar, los huevos y la
    vainilla.

2.  Se ciernen harina, sal, bicarbonato, avena y canela en una mezcla
    homogenea seca.

3.  Se incorpora la mezcla seca a la humeda poco a poco y se agregan las
    pasitas o arandanos (o nada).

4.  Se engrasa una charola y se hacen bolitas de 1 pulgada de diametro
    aproximadamente, sin aplastar se acomodan a 2 pulgadas de distancia
    y se hornean por ~20 min a 180° (350°F).
