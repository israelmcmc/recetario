default:
	$(MAKE) md2rst
	$(MAKE) fixrefs
	$(MAKE) html
	$(MAKE) ebook

# recommonmark doesn't support some features like footnotes
md2rst:
	for f in recetas/*.md; do \
	pandoc $${f} --wrap=preserve -o $${f%.*}.rst ; \
	done

# Refs to files are not interpreted properly
fixrefs:
	for f in recetas/*.rst; do \
	perl -pi -e 's|`(.*?) <(.*?).md>`__|:doc:`\1 <\2>`|g' $${f} ; \
	done

html:
	sphinx-build -b html ./ public
	#Because we converted from md to rst first
	for f in public/recetas/*.html; do \
	perl -pi -e 's|https://gitlab.com/israelmcmc/recetario/blob/master/recetas/(.*?).rst|https://gitlab.com/israelmcmc/recetario/blob/master/recetas/\1.md|g' $${f} ; \
	done

ebook:
	$(MAKE) figures-lowres
	sphinx-build -b epub ./ public/ebook
	ebook-convert public/ebook/Recetario.epub public/ebook/Recetario.azw3
	ebook-convert public/ebook/Recetario.epub public/ebook/Recetario.pdf --pdf-default-font-size 14
	$(MAKE) figures-restore

figures-lowres:
	cp -r figures figures_backup
	for fig in figures/*; do \
	convert $${fig}  -resize 1024x768\> $${fig}; \
	done

figures-restore:
	mv figures_backup/* figures
	rmdir figures_backup
